'use strict';

module.exports = {
    guide: "Guide",
    guideHint: "Click here to show hints",
    guideSkip: "Click here to go to the next level!",
    guideSetting: "Click here to open settings",
    guideList: "You can replay an unlocked level here",
    guidePencil: "Choose your favorite pen here",
    loading: "Loading...",
    level: "Level ",
    levels: "Levels",
    next: "NEXT",
    settings: "Settings",
    pencils: "Pencils",
    use: "use",
    used: "used",
    free: "Free",
    music: "Music:",
    sound: "Sound:",
    on: "ON",
    off: "OFF"
};