const {ccclass, property} = cc._decorator;

const anim_name = {
    incorrect: "incorrect",
    correct: "correct"
}
@ccclass
export default class Incorrect extends cc.Component {

    onEnable(): void {
        if (this.node.name == "Correct") {
            this.node.getComponent(cc.Animation).play(anim_name.correct);
        }
        else {
            this.node.getComponent(cc.Animation).play(anim_name.incorrect);
        }
    }

    hide(): void {
        this.node.active = false;
    }
}
