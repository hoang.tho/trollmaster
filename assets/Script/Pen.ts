import GameManager from './GameManager';
const {ccclass, property} = cc._decorator;

@ccclass
export default class Pen extends cc.Component {

    @property(cc.SpriteAtlas)
    pencils: cc.SpriteAtlas = null;

    private _gameManager = null;

    onLoad(): void {
        this._gameManager = cc.find("Canvas/GameContainer").getComponent("GameManager");
    }
    onEnable(): void {
        this.node.angle = -30;
        cc.tween(this.node).repeatForever(
                cc.tween().by(0.3, {angle: -5}).by(0.3, {angle: 5})
            ).start();
        let index = GameManager.userData.pencil;
        this.node.getComponent(cc.Sprite).spriteFrame = this.pencils.getSpriteFrame("p" + (index + 1));
    }

    onDisable(): void {
        cc.Tween.stopAllByTarget(this.node);
    }
}
