import BundleManager from "./BundleManager";
import Data from "./Data";
import { KEY } from "./Data";
import Loading from "./Loading";
import { TOTAL_LEVEL } from "./GameManager";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Splash extends cc.Component {
    
    @property(Loading)
    loading: Loading = null;

    public static userData = null;
    private _percent = 0;

    onLoad(): void {
        Data.getData(KEY.FACEBOOK, (error, data) => {
            error && console.error(error);
            Splash.userData = data;
            let level = data.unlock + 1;
            console.time("preloadScene");
            cc.director.preloadScene("Game",
            (finish, total) => {
                if (this._percent <= finish / total)
                    this._percent = finish / total;
                this.loading.updateProgress(this._percent);
    
            },
            (err) => {
                console.timeEnd("preloadScene");
                if (err) {
                    cc.error(err);
                }
                console.time("loadScene");
                cc.director.loadScene("Game", () => {
                    console.timeEnd("loadScene");
                });
            });
            console.time("bundle");
            BundleManager.getInstance().loadBundle("resources", () => {
                BundleManager.getInstance().loadLevelSequence(
                    level,
                    (level + 2) > TOTAL_LEVEL ? level + (TOTAL_LEVEL - level) : level + 2,
                    null,
                    () => {
                        console.timeEnd("bundle");
                    }
                );
            });
        });
    }
}
