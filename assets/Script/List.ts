import Panel from "../Script/Panel";
import { KEY } from "../Script/Data";
import { TOTAL_LEVEL } from "../Script/GameManager";
import Data from "../Script/Data";
import { STATE } from "./Click";
import GameManager from "./GameManager";



const {ccclass, property} = cc._decorator;

@ccclass
export default class List extends Panel {

    @property([cc.SpriteAtlas])
    icon: cc.SpriteAtlas[] = [];
    private _size = null;
    private _data = null;
    private _current = null;
    private _view = null;

    onLoad(): void {
        this.initList();
    }

    onEnable(): void {
        super.onEnable();
        if (GameManager.getInstance()._current.level) {
            this._data = GameManager.userData;
            this.node.on("onclick", this.onClick, this);
            this.setState();
            this._view = this.container.getChildByName("view");
            this.scrollToCurrent();
        }
        else {
            this.node.active = false;
        }
    }

    onClick(event): void {
        if (event.level != this._current.node.name) {
            GameManager.getInstance().activeLevel(event.level);
            //Off node hiện tại
            this._current.setState(STATE.UNLOCK);
            //Gán node mới
            this._current = event.target.getComponent("Click");
            // Active node mới
            this._current.setState(STATE.CURRENT);
        }
        this.onClose();
    }

    initList(): void {
        this.content.removeAllChildren(true);
        let pool = GameManager.getInstance()._itemPool;
        for (let i = 0; i < TOTAL_LEVEL; ++i)
        {
            let item = null;
            if (pool.size() > 0) {
                item = pool.get();
            }
            else {
                item = cc.instantiate(this.item);
            }
            item.name = (i + 1).toString();
            this.content.addChild(item);
        }
    }

    onClickSetting(): void {
        GameManager.getInstance().onClickSetting();
    }

    setState(): void {
        let atlas = null;
        for (let i = 0; i < this._data.unlock + 1; ++i)
        {
            let item = this.content.children[i];
            item.getChildByName("number").active = false;
            let script = item.getComponent("Click");
            script._clicked = false;
            script.setState(STATE.UNLOCK);
            if (i < 25) {
                atlas = this.icon[0];
            }
            else if (i < 44) {
                atlas = this.icon[1];
            }
            else if (i < 63) {
                atlas = this.icon[2];
            }
            else if (i < 90) {
                atlas = this.icon[3]
            }
            else if (i < 125) {
                atlas = this.icon[4];
            }
            else if (i < 166) {
                atlas = this.icon[5];
            }
            else if (i < 195) {
                atlas = this.icon[6];
            }
            else if (i < 240) {
                atlas = this.icon[7];
            }
            else if (i < 262) {
                atlas = this.icon[8];
            }

            let arr = [18, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 131, 132, 133, 134, 135,
                136, 137, 138, 139, 141, 143, 145, 148, 157, 161, 163, 165, 167, 169, 170, 171, 173, 185,
                186, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 214, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223,
                224, 225, 227, 229, 231, 233, 235, 237, 239, 241, 243, 245, 246, 247, 248, 249];
            if (arr.includes(i)) {
                item.getChildByName("content").scale = 1.05;
            }
            else {
                item.getChildByName("content").scale = 0.45; 
            }

            let arr2 = [200, 202, 204, 206, 208, 213, 215, 216, 217, 218, 219, 220];
            if (arr2.includes(i)) {
                item.getChildByName("content").scale = 0.5;
            }

            item.getChildByName("content").active = true;
            item.getChildByName("content").getComponent(cc.Sprite).spriteFrame = atlas.getSpriteFrame(i + 1);
        }
        this._current = this.content.children[GameManager.getInstance()._current.level - 1].getComponent("Click");
        this._current._clicked = false;
        this._current.setState(STATE.CURRENT);
        for (let i = this._data.unlock + 1; i < TOTAL_LEVEL; ++i)
        {
            let item = this.content.children[i];
            item.getChildByName("number").active = true;
            item.getChildByName("number").getComponent(cc.Label).string = i + 1;
            item.getChildByName("content").active = false;
            let script = item.getComponent("Click");
            script._clicked = true;
            script.setState(STATE.LOCK);
        }
    }

    scrollToCurrent(): void {
        let index = this.content.children.indexOf(this._current.node);
        let length = this.content.children.length;
        let percent = 1 - index / length;
        this.container.getComponent(cc.ScrollView).scrollToPercentVertical(percent + 1 / (TOTAL_LEVEL / 5), 0.5);
    }
}
