import FBInstantManager from './FBInstantManager';

const {ccclass, property} = cc._decorator;


const LOCAL_KEY = "troll_master_data";

export enum KEY {
    LOCAL = "troll_master_local_data",
    FACEBOOK = "troll_master_facebook_data"
}

const serilizeData = (data) => {
    return data;
}

const defaultData = {
    unlock: 0,
    isMusic: true,
    isSound: true,
    isVibrate: true,
    pencil: 0,
    pencils: [true, false, false, false, false, false, false, false, false, false, false, false, false, false, false],
    firstPlay: null
}

@ccclass
export default class Data {
    
    public static saveData(key: KEY, data: JSON, callback: Function = null): void {
        if (key == KEY.FACEBOOK)
        {
            FBInstantManager.getInstance().setPlayerData(data, (error) => {
                error && console.error(error);
                Data.saveData(KEY.LOCAL, data);
                callback && callback();
            });
        }
        else if (key == KEY.LOCAL)
        {
            cc.sys.localStorage.setItem(KEY.LOCAL, JSON.stringify(data));
            callback && callback();
        }
    }

    public static getData(key: KEY, callback: Function = null): void {
        if (key == KEY.LOCAL)
        {
            let data = JSON.parse(cc.sys.localStorage.getItem(KEY.LOCAL));
            if (data)
            {
                return callback && callback(data);
            }
            return callback && callback(defaultData);
        }
        else if (key == KEY.FACEBOOK)
        {
            this.getDataFacebook(callback);
        }
    }

    public static removeData(key: KEY): void {
        cc.sys.localStorage.removeItem(key);
    }

    public static getDataFacebook(callback: Function = null): void {
        FBInstantManager.getInstance().getPlayerData((error, data) => {
            if (error) {
                console.log('get facebook data err', error);
                const data = cc.sys.localStorage.getItem(KEY.LOCAL);

                if (data) {
                    return callback(null, serilizeData(JSON.parse(data)))
                }

                return callback(null, { ...defaultData })
            }
            if (!data)
            {
                return Data.getData(KEY.LOCAL, (localData) => {
                    callback(
                        error,
                        localData
                    )
                })
            }
            return callback && callback(error, {...defaultData, ...data});
        });
    }
}
