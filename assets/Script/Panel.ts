import Data from "../Script/Data";
import { KEY } from "../Script/Data";
import { SOUND } from "./GameManager";
import GameManager from "./GameManager";


const {ccclass, property} = cc._decorator;

enum Anim {
    HideContainer = "hide_container",
    ShowContainer = "show_container",
    HideTop = "hide_top",
    ShowTop = "show_top"
}

@ccclass
export default class Panel extends cc.Component {

    @property(cc.Node)
    container: cc.Node = null;

    @property(cc.Node)
    topButton: cc.Node = null;

    @property(cc.Node)
    content: cc.Node = null;

    @property(cc.Prefab)
    item: cc.Prefab = null;

    userData = null;
    size = null;

    onEnable(): void {
        this.addEventListener();
        this.userData = GameManager.userData;
        this.onOpen();
    }

    onDisable(): void {
        this.removeEventListener();
    }
    addEventListener(): void {
        this.node.on("itemclick", this.onClickItem, this);
    }

    removeEventListener(): void {
        this.node.off("itemclick", this.onClickItem, this);
    }

    onClickItem(event): void {
    }
    
    saveData(key: KEY, data): void {
        Data.saveData(key, data);
    }

    onClose(): void {
        GameManager.getInstance().playSound(SOUND.CLICK, false);
        this.topButton.getComponent(cc.Animation).play(Anim.HideTop);
        this.container.getComponent(cc.Animation).play(Anim.HideContainer);
        this.scheduleOnce(() => {
            this.close();
        }, 0.5);
    }

    close(): void {
        this.node.active = false;
    }

    onOpen(): void {
        this.topButton.getComponent(cc.Animation).play(Anim.ShowTop);
        this.container.getComponent(cc.Animation).play(Anim.ShowContainer);
    }

    update(dt): void {
        this.size = cc.view.getCanvasSize();
        if (this.size.width == 375 && this.size.height == 812) {
            this.topButton.scale = 0.9;
            this.container.scale = 0.9;
        }
        else {
            this.topButton.scale = 1;
            this.container.scale = 1;
        }
    }
}
