import Touch from "./Touch";
import {SOUND} from "./GameManager";
import GameManager from "./GameManager";
import { GroupUI } from "./UIController";
import UIController from "./UIController";


const {ccclass, property} = cc._decorator;

@ccclass
export default class LevelController extends cc.Component {

    @property({type: cc.Component.EventHandler})
    init: cc.Component.EventHandler[] = [];

    @property({type: cc.Component.EventHandler})
    complete: cc.Component.EventHandler[] = [];

    @property([sp.Skeleton])
    skeleton: sp.Skeleton[] = [];

    @property
    level = 0;

    @property(cc.Label)
    label: cc.Label = null;
    
    @property([cc.Node])
    inside: cc.Node[] = [];

    @property(cc.Node)
    block: cc.Node = null;

    @property(Touch)
    touch: Touch = null;

    @property(cc.Node)
    outside: cc.Node = null;

    @property(cc.Prefab)
    pen: cc.Prefab = null;

    private _valid = false;
    private _gameManager = null;
    private _circle = [];
    private _temp = [];

    private _hint = false;
    private _pen = null;
    private _incorrect = null;
    private _correct = null;

    onLoad(): void {
        GameManager.getInstance()
        this.label.node.active = false;
    }

    onEnable(): void {
        GameManager.getInstance()._current = {
            level: this.level,
            node: this.node
        }
        GameManager.getInstance().setLevel(this.level);

        this._hint = false;
        
        this.node.on("valid", this.onValid, this);
        this.node.on("draw_complete", this.onComplete.bind(this), this);
        this.node.on("draw_start", this.onStartDraw, this);
        this.reset();
        this.block.active = false;

        this._pen = this.node.getChildByName("Pen");
        if (!this._pen) {
            this._pen = cc.instantiate(this.pen);
            this.node.addChild(this._pen);
        }
        this._pen.active = false;
        this._pen.zIndex = cc.macro.MAX_ZINDEX;

        // UIController.getInstance().playHintIdle();
    }

    start(): void {
        GameManager.getInstance().preloadLevel(this.level);
    }

    onStartDraw(): void {
        this._circle = [];
        this._temp = this.inside;
        if (this._pen) {
            this._pen.active = true;
        }
        else {
            this._pen.active = true;
        }
    }

    onValid(event): void {
        this._valid = event.valid;
    }

    onComplete(event): void {
        this._pen.active = false;
        if (this._hint) {
            this.inside.forEach(node => {
                node.opacity = 255;
            })
        }
        if (this._valid) {
            GameManager.getInstance().playSound(SOUND.CORRECT1, false);
            UIController.getInstance().showUI(GroupUI.InGame, false);
            this.inside.forEach(node => {
                node.opacity = 0;
            });

            this.scheduleOnce(() => {
                GameManager.getInstance().playSound(SOUND.WIN, false);
            }, 0.7);

            let component = this.complete[0]["_componentName"];
            let handler = this.complete[0].handler;
            let func = this.complete[0].target.getComponent(component)[handler];
            func(this.complete[0].target.getComponent(component));

            GameManager.getInstance()._block = true;
            this._circle.forEach(element => {
                element.opacity = 0;
            })

            this.block.active = true;

            this.showParticle();
            this.showCorrect();
        }
        else
        {
            GameManager.getInstance().playSound(SOUND.FALSE, false);
            this.showIncorrect();
        }
    }

    onFinish(e): void {
        e.node.getComponent("LevelController").block.active = true;
        let event = new cc.Event.EventCustom("onfinish", true);
        e.node.dispatchEvent(event);
    }

    reset(): void {
        this.inside.forEach(element => {
            element.opacity = 0;
        })
        this.block.active = false;
        let component = this.init[0]["_componentName"];
        let handler = this.init[0].handler;
        let func = this.init[0].target.getComponent(component)[handler];
        func(this.init[0].target.getComponent(component));
    }

    onHint(): void {
        this.inside.forEach(element => {
            element.opacity = 255;
        })
    }

    showParticle(): void {
        let particle = cc.instantiate(GameManager.getInstance().star);
        particle.x = this.inside[0].x;
        particle.y = this.inside[0].y;
        particle.zIndex = cc.macro.MAX_ZINDEX;
        this.node.addChild(particle);
    }

    showIncorrect(): void {
        if (!this._incorrect) {
            let prefab = GameManager.getInstance().incorrect;
            this._incorrect = cc.instantiate(prefab);
            this.node.addChild(this._incorrect);
        }
        this._incorrect.active = true;
    }

    showCorrect(): void {
        if (!this._correct) {
            let prefab = GameManager.getInstance().correct;
            this._correct = cc.instantiate(prefab);
            this.node.addChild(this._correct);
        }
        this._correct.active = true;
    }
}
