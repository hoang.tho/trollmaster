import Data, { KEY } from './Data';
const {ccclass, property} = cc._decorator;


export enum GroupUI {
    InGame,
    EndGame
}

enum Anim {
    HideTop = "hide_top",
    ShowTop = "show_top",
    HideBot = "hide_bot",
    ShowBot = "show_bot",
    HideNext = "hide_next",
    ShowNext = "show_next",
    HideHint = "hide_hint",
    ShowHint = "show_hint",
    HintIdle = "hint"
}

@ccclass
export default class UIController extends cc.Component {

    public static _instance = null;

    @property(cc.Node)
    replay: cc.Node = null;

    @property(cc.Node)
    textLevel: cc.Node = null;

    @property(cc.Node)
    skip: cc.Node = null;

    @property(cc.Node)
    setting: cc.Node = null;

    @property(cc.Node)
    next: cc.Node = null;

    @property(cc.Node)
    invite: cc.Node = null;

    @property(cc.Node)
    share: cc.Node = null;

    @property(cc.Node)
    pencil: cc.Node = null;

    @property(cc.Node)
    list: cc.Node = null;

    @property(cc.Node)
    hint: cc.Node = null;

    cheatCount = 0;
    public static getInstance(): UIController {
        if (!UIController._instance)
        {
            UIController._instance = this;
        }
        return UIController._instance;
    }

    onLoad(): void {
        UIController._instance = this;
    }

    start(): void {
        this.textLevel.on(cc.Node.EventType.TOUCH_END, () => {
            ++this.cheatCount;
            if (this.cheatCount == 9)
            {
                Data.saveData(KEY.FACEBOOK, JSON.parse("{}"));
            }
        }, this);
    }

    setLevel(level: number): void {
        this.textLevel.getComponent(cc.Label).string = "LEVEL " + level;
    }

    showInGame(isShow?: Boolean): void {
        if (isShow)
        {
            this.skip.getComponent(cc.Animation).play(Anim.ShowTop);
            this.setting.getComponent(cc.Animation).play(Anim.ShowTop);
            this.textLevel.getComponent(cc.Animation).play(Anim.ShowTop);
            this.list.getComponent(cc.Animation).play(Anim.ShowBot);
            this.pencil.getComponent(cc.Animation).play(Anim.ShowBot);
            let animState = this.hint.getComponent(cc.Animation).play(Anim.ShowHint);
            this.scheduleOnce(() => {
                this.playHintIdle();
            }, animState.duration);
        }
        else
        {
            this.skip.getComponent(cc.Animation).play(Anim.HideTop);
            this.setting.getComponent(cc.Animation).play(Anim.HideTop);
            this.textLevel.getComponent(cc.Animation).play(Anim.HideTop);
            this.list.getComponent(cc.Animation).play(Anim.HideBot);
            this.pencil.getComponent(cc.Animation).play(Anim.HideBot);
            this.hint.getComponent(cc.Animation).play(Anim.HideHint);
        }
    }

    showEndGame(isShow?: Boolean): void {
        if (isShow)
        {
            this.replay.getComponent(cc.Animation).play(Anim.ShowTop);
            this.next.getComponent(cc.Animation).play(Anim.ShowNext);
            this.invite.getComponent(cc.Animation).play(Anim.ShowNext);
            this.share.getComponent(cc.Animation).play(Anim.ShowNext);
            this.textLevel.getComponent(cc.Animation).play(Anim.ShowTop);
        }
        else
        {
            this.replay.getComponent(cc.Animation).play(Anim.HideTop);
            this.next.getComponent(cc.Animation).play(Anim.HideNext);
            this.invite.getComponent(cc.Animation).play(Anim.HideNext);
            this.share.getComponent(cc.Animation).play(Anim.HideNext);
        }
        const gameManager = cc.find('Canvas/GameContainer').getComponent('GameManager');
        this.share.active = gameManager.canShare();
        this.invite.active = gameManager.canInvite();
    }

    showUI(group?: GroupUI, isShow?: Boolean): void {
        switch (group)
        {
            case GroupUI.InGame:
                this.showInGame(isShow);
                break;
            case GroupUI.EndGame:
                this.showEndGame(isShow);
                break;
        }
    }

    playHintIdle(): void {
        let hintAnim = this.hint.getComponent(cc.Animation).play(Anim.HintIdle);
    }

    pauseHintIdle(): void {
        let hintAnim = this.hint.getComponent(cc.Animation).pause(Anim.HintIdle);
    }
    
}
