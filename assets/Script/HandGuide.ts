const {ccclass, property} = cc._decorator;

@ccclass
export default class HandGuide extends cc.Component {

    @property(cc.Node)
    hand: cc.Node = null;

    complete(): void {
        this.hand.active = true;
        this.hand.x = -this.node.convertToNodeSpaceAR(this.hand.parent.convertToWorldSpaceAR(this.node.position)).x;
        this.hand.getComponent(cc.Animation).play("Guide2");
        this.node.getComponent(cc.Animation).play("hint");
    }
}
