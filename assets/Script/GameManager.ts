import UIController from './UIController';
import BundleManager from "./BundleManager";
import Data from "./Data";
import { KEY } from "./Data";
import AdsManager from "./AdsManager";
import Loading from "./Loading";
import FBInstantManager from './FBInstantManager';
import { GroupUI } from "./UIController";
import {GameAnalytics} from "gameanalytics";
import {EGAProgressionStatus} from "gameanalytics";
import {EGAAdAction, EGAAdError, EGAAdType} from './Analytics';
import Splash from './Splash';


const {ccclass, property} = cc._decorator;
const VERSION = '1.1.11'
var loadedBanner = false;
var countLoad = 0;

export enum GROUP {
    DEFAULT,
    INSIDE,
    OUTSIDE,
    TOUCH
}

export const TOTAL_LEVEL = 250;

export enum SOUND {
    BG,
    CLICK,
    DRAW,
    CORRECT,
    CORRECT1,
    CORRECT2,
    WIN,
    FALSE
}

let GAME_KEY = "05600c407856ea4f193272ed083e2a70";
let SECRET_KEY = "b8f6dab0d2ba2011bb31775ff57ec19019c39182";

@ccclass
export default class GameManager extends cc.Component {

    @property(cc.Prefab)
    setting: cc.Prefab = null;

    @property([cc.AudioClip])
    sounds: cc.AudioClip[] = [];

    @property(Loading)
    loading: Loading = null;

    @property(cc.Prefab)
    star: cc.Prefab = null;

    @property(cc.Prefab)
    item: cc.Prefab = null;

    @property(cc.Prefab)
    incorrect: cc.Prefab = null;
    
    @property(cc.Prefab)
    correct: cc.Prefab = null;
    
    @property(cc.Prefab)
    guide: cc.Prefab = null;

    public _itemPool = null;
    public _block = false;
    public _current = {
        level: null,
        node: null
    };

    public static userData = null;

    private _bundle;
    private _loadLevel = null;
    private _list = null;
    private _setting = null;
    private _pencil = null;

    private static _instacne = null;

    public static getInstance(): GameManager {
        if (!GameManager._instacne) {
            GameManager._instacne = this;
        }
        return GameManager._instacne;
    }

    start(): void {
        GameManager._instacne = this;

        cc.director.getCollisionManager().enabled = true;
        this._bundle = BundleManager.getInstance();
        GameManager.userData = Splash.userData;
        this._loadLevel = GameManager.userData.unlock + 3;
        console.log('GAME VERSION: ', VERSION)

        this._itemPool = new cc.NodePool();
        for (let i = 0; i < TOTAL_LEVEL; ++i) {
            let item = cc.instantiate(this.item);
            this._itemPool.put(item);
        }

        if (GameManager.userData.isMusic)
            this.playBG();

        let level = GameManager.userData.unlock + 1 >= TOTAL_LEVEL ? TOTAL_LEVEL - 1 : GameManager.userData.unlock;

        this._bundle.getPrefabByName("Level" + (level + 1), null, (err, prefab) => {
            if (err) {
                console.error(err);
            }
            else {
                let node = cc.instantiate(prefab);
                this.node.addChild(node);
                
                if (this.checkFirstPlay() && GameManager.userData.unlock == 0) {
                    let guide = cc.instantiate(this.guide);
                    cc.find("Canvas").addChild(guide);
                    node.active = false;
                }
            }
        });

        UIController.getInstance().showUI(GroupUI.InGame, true);
        UIController.getInstance().showUI(GroupUI.EndGame, false);
        this.loading.node.active = false;
        this.loading.node.zIndex = cc.macro.MAX_ZINDEX;

        try {
            AdsManager.getInstance().preload();

        }
        catch (e) {
            console.log(e);
        }

        try
        {
            FBInstantManager.getInstance().checkSupportedApis([]);
            this.loadBanner();
        }
        catch (e)
        {
            console.error(e);
        }
        GameAnalytics.setEnabledInfoLog(true);
        GameAnalytics.setEnabledVerboseLog(true);
        GameAnalytics.configureBuild(VERSION)
        GameAnalytics.configureUserId(FBInstantManager.getInstance().getPlayerId())
        GameAnalytics.initialize(GAME_KEY, SECRET_KEY);
    }

    onEnable(): void {
        this.node.on("onfinish", this.onFinish, this);

        // add user for sending message
        this.askSubcribeBot(() => {
            this.askCreateShortcut()
        })
    }

    onFinish(): void {
        let data = GameManager.userData;

        if (this._current.level == data.unlock + 1 && data.unlock + 1 < TOTAL_LEVEL) {
            ++GameManager.userData.unlock;
        }
        Data.saveData(KEY.FACEBOOK, data, () => {
            UIController.getInstance().showUI(GroupUI.EndGame, true);
            GameManager.logPassed(data.unlock);
        });
    }

    onNextLevel(event): void {
        this.playSound(SOUND.CLICK, false);
        this.loadBanner();
        if (event.target.name == "Skip") {
            AdsManager.getInstance().showRewardedAds(err => {
                if (GameManager.userData.isMusic)
                    cc.audioEngine.resumeMusic();
                    
                if (err) {
                    console.error(err);
                    if (err.code != "ADS_NOT_LOADED") {
                        GameManager.logErrorRewardAds(EGAAdError.Unknown)
                        return;
                    }
                    GameManager.logErrorRewardAds(EGAAdError.NoFill)
                }
                else {
                    GameManager.logErrorRewardAds()
                    if (GameManager.userData.unlock < TOTAL_LEVEL - 1) {
                        this.nextLevel(event);
                    }
                }
            });
        }
        else {
            //Từ level 4 trở đi mới hiển thị quảng cáo
            if (GameManager.userData.unlock > 3) {
                AdsManager.getInstance().showInterestialAds(e => {
                    if (e) {
                        console.error(e.message);
                        let logError = EGAAdError.Unknown
                        try {
                            if (e.code === 'ADS_NOT_LOADED' || e.code === 'RATE_LIMITED') {
                                logError = EGAAdError.NoFill
                            }
                        } catch (exc) {
                            console.log("error error", exc)
                        }
                        GameManager.logErrorInterestialAds(logError)
                    } else {
                        GameManager.logShowInterestialAds()
                    }
                    this.nextLevel(event);
                });
            }
            else {
                this.nextLevel(event);
            }
        }
    }


    nextLevel(event): void {
        if (this._current.level == TOTAL_LEVEL) {
            return;
        }

        GameManager.logNextLevel(this._current.level)

        UIController.getInstance().showUI(GroupUI.InGame, true);
        UIController.getInstance().showUI(GroupUI.EndGame, false);

        //Ẩn node hiện tại
        this._current.node.active = false;

        //Kiểm tra node đó đã tạo hay chưa, tạo rồi thì active
        let next = this.node.getChildByName("Level" + (this._current.level + 1));

        if (next) {
            this._block = false;
            next.active = true;
            return;
        }

        //Kiểm tra level đã unlock hay chưa, chưa unlock thì +1 rồi unlock
        let level;
        if (this._current.level == GameManager.userData.unlock + 1) {
            level = ++GameManager.userData.unlock;
            ++level;
        }
        else {
            level = this._current.level + 1;
        }

        //Kiểm tra người chơi click Skip thì phải update lại level unlock
        if (event.target.name == "Skip") {
            Data.saveData(KEY.FACEBOOK, GameManager.userData);
        }

        this._bundle.getPrefabByName("Level" + level, (finish, total) => {
                this.loading.node.active = true;
                this.loading.updateProgress(finish / total);
            }, (err, prefab) => {
                if (err) {
                    console.error(err);
                }
                else {
                    let level = cc.instantiate(prefab);
                    this.loading.node.active = false;
                    this.node.addChild(level);
                    this._block = false;
                    this.preloadLevel(GameManager.userData.unlock + 1);
                }
        });
    }

    preloadLevel(current: number): void {
        this._bundle.getPrefabByName("Level" + (current + 1));
    }

    onReplay(): void {
        this.playSound(SOUND.CLICK, false);
        AdsManager.getInstance().showInterestialAds(e => {
            if (e) {
                let logError = EGAAdError.Unknown
                try {
                    if (e.code === 'ADS_NOT_LOADED' || e.code === 'RATE_LIMITED') {
                        logError = EGAAdError.NoFill
                    }
                } catch (exc) {
                    console.log("error error", exc)
                }
                GameManager.logErrorInterestialAds(logError)
            } else {
                GameManager.logShowInterestialAds()
            }
            this._current.node.getComponent("LevelController").reset();
            this._current.node.getComponent("LevelController")._hint = false;
            this._block = false;
            UIController.getInstance().showUI(GroupUI.InGame, true);
            UIController.getInstance().showUI(GroupUI.EndGame, false);
        });
    }

    activeLevel(level): void {
        //Preload level sau
        this.preloadLevel(level);

        //Tim level xem da co hay chua
        let lv = "Level" + level;
        for (let l of this.node.children)
        {
            if (l.name == lv) {
                this._current.node.active = false;
                l.active = true;
                return;
            }
        }
        //Chua co thi load bundle
        this._bundle.getPrefabByName(lv, (finish, total) => {
                this.loading.node.active = true;
                this.loading.updateProgress(finish / total);
            },(err, prefab) => {
                if (err) {
                    console.error(err);
                }
                else {
                    this.loading.node.active = false;
                    this._current.node.active = false;
                    let level = cc.instantiate(prefab);
                    this.node.addChild(level);
                }
            }
        );
    }

    onClickList(): void {
        this.playSound(SOUND.CLICK, false);

        if (this._list) {
            this._list.active = true;
            this._list.zIndex = cc.macro.MAX_ZINDEX;
            return;
        }

        let percent = 0;
        this.loading.node.active = true;
        console.time("preload_list");
        cc.resources.preload("List/List",
            cc.Prefab,
            (finish, total) => {
                if (percent <= finish / total)
                    percent = finish / total;
                this.loading.updateProgress(percent);
            },
            (err) => {
            console.timeEnd("preload_list");
            if (!err)
            console.time("load_list");
            cc.resources.load("List/List", (err, prefab) => {
                if (!err) {
                    console.timeEnd("load_list");
                    this._list = cc.instantiate(prefab);
                    this._list.zIndex = cc.macro.MAX_ZINDEX;
                    cc.find("Canvas").addChild(this._list);
                    this.loading.node.active = false;
                }
            })
        })

    }

    onShowHint(event): void {
        this.playSound(SOUND.CLICK, false);
        AdsManager.getInstance().showRewardedAds(err => {
            if (err) {
                console.error(err);
                if (err.code != "ADS_NOT_LOADED") {
                    GameManager.logErrorRewardAds(EGAAdError.Unknown)
                    return;
                }
                GameManager.logErrorRewardAds(EGAAdError.NoFill)
            } else {
                GameManager.logReceiveRewardAds()
            }
            GameManager.logEvent("UseHint");
            UIController.getInstance().pauseHintIdle();
            if (GameManager.userData.isMusic)
            {
                cc.audioEngine.resumeMusic();
            }

            if (!this._block) {
                this.showHint(event);
            }
        });
    }

    showHint(event?: cc.Event): void {
        event && (event.target.opacity = 200);
        this._current.node.getComponent("LevelController").onHint();
        this._current.node.getComponent("LevelController")._hint = true;
    }

    onClickPencil(): void {
        this.playSound(SOUND.CLICK, false);
        if (this._pencil) {
            this._pencil.active = true;
            this._pencil.zIndex = cc.macro.MAX_ZINDEX;
            return;
        }
        let percent = 0;
        this.loading.node.active = true;
        cc.resources.preload("Pencils/Pencils",
            cc.Prefab,
            (finish, total) => {
                if (percent < finish / total)
                    percent = finish / total;
                this.loading.updateProgress(percent);
            },
            (err) => {
                if (!err) {
                    cc.resources.load("Pencils/Pencils", cc.Prefab, (err ,prefab) => {
                        if (!err) {
                            this._pencil = cc.instantiate(prefab);
                            this._pencil.zIndex = cc.macro.MAX_ZINDEX;
                            cc.find("Canvas").addChild(this._pencil);
                            this.loading.node.active = false;
                        }
                    })
                }
            })
    }

    onClickSetting(): void {
        this.playSound(SOUND.CLICK, false);
        if (this._setting) {
            this._setting.active = true;
        }
        else {
            this._setting = cc.instantiate(this.setting);
            cc.find("Canvas").addChild(this._setting);
        }
    }

    playBG(): void {
        cc.audioEngine.playMusic(this.sounds[SOUND.BG], true);
    }

    playSound(sound: SOUND, loop: boolean = null): number {
        if (GameManager.userData.isSound)
            return cc.audioEngine.playEffect(this.sounds[sound], loop);
        return -1;
    }

    setLevel(level: number): void {
        UIController.getInstance().setLevel(level);
        GameManager.logLevel(level - 1);
    }
    
    onShare(e=null) {
        GameManager.logEvent("ShareClick")
        FBInstantManager.getInstance().shareGame()
    }

    onInvite(e=null) {
        GameManager.logEvent("InviteClick")
        FBInstantManager.getInstance().inviteAsync();
    }

    canShare() {
        return FBInstantManager.getInstance().canShareGame()
    }

    canInvite() {
        return FBInstantManager.getInstance().canInvitePlay()
    }

    askCreateShortcut(cb?) {
        FBInstantManager.getInstance().getPlayerData((err, data) => {
            if (err) {
                cb && cb()
                return console.log('--- fb instant get data err', err)
            }

            if (!data) {
                data = {
                    hadCreatedShortcut: false,
                    visited: 1,
                }
            }

            if (!data.visited) {
                data.visited = 1
            } else {
                data.visited++
            }

            if (data.hadCreatedShortcut || data.visited < 2) return

            FBInstantManager.getInstance().createShortcut((result) => {
                if (result) {
                    data.hadCreatedShortcut = true
                    FBInstantManager.getInstance().setPlayerData(data, (err) => {
                        err && console.log('--- fb instant set data err', err)
                    })
                }
                cb && cb()
            })
        })
    }

    askSubcribeBot(cb?) {
        FBInstantManager.getInstance().subcribeBot((result) => {
            // save on user data
            cb && cb()
        })
    }

    checkFirstPlay(): Boolean {
        let first = GameManager.userData.firstPlay;
        if (first)
            return false;
        GameManager.userData.firstPlay = new Date().getTime();
        Data.saveData(KEY.FACEBOOK, GameManager.userData);
        return true;
    }

    
    static logEvent(eventName: string) {
        GameAnalytics.addDesignEvent(eventName)
    }
    static logFailed(level: number) {
        // Current not have
        GameAnalytics.addProgressionEvent(EGAProgressionStatus.Fail,`Level${level+1}`)
    }
    static logPassed(level: number) {
        GameAnalytics.addProgressionEvent(EGAProgressionStatus.Complete,`Level${level+1}`)
    }
    static logLevel(level: number) {
        GameManager.logEvent(`Enter_Level_${level+1}`)
        GameAnalytics.addProgressionEvent(EGAProgressionStatus.Start,`EnterLevel${level+1}`)

    }
    static logNextLevel(level: number) {
        GameManager.logEvent(`NextLevel${level}`)
        GameAnalytics.addProgressionEvent(EGAProgressionStatus.Start,`NextLevel${level+1}`)
    }
    static logReceiveRewardAds() {
        const placeId = AdsManager.getInstance().getRewardPlacementId()
        GameAnalytics.addAdEvent(EGAAdAction.RewardReceived, EGAAdType.RewardedVideo, 'facebookads', placeId)
    }
    static logErrorRewardAds(error: EGAAdError=EGAAdError.Unknown) {
        /**
         * EGAAdError.NoFill if ads not load, EGAAdError.Unknown if ads close by user
         * */
        const placeId = AdsManager.getInstance().getRewardPlacementId()
        GameAnalytics.addAdEventWithNoAdReason(EGAAdAction.FailedShow, EGAAdType.RewardedVideo, 'facebookads', placeId, error)
    }
    static logShowInterestialAds() {
        const placeId = AdsManager.getInstance().getInsterestialPlacementId()
        GameAnalytics.addAdEvent(EGAAdAction.Show, EGAAdType.Interstitial, 'facebookads', placeId)
    }
    static logErrorInterestialAds(error: EGAAdError=EGAAdError.Unknown) {
        /**
         * EGAAdError.NoFill if ads not load, EGAAdError.Unknown if other reason
         * */
        const placeId = AdsManager.getInstance().getInsterestialPlacementId()
        GameAnalytics.addAdEventWithNoAdReason(EGAAdAction.FailedShow, EGAAdType.Interstitial, 'facebookads', placeId, error)
    }

    loadBanner(): void {
        if (!loadedBanner)
        {
            ++countLoad;
            if (countLoad > 3)
                return;
            let size = cc.view.getCanvasSize();
            AdsManager.getInstance().loadBannerAds(error => {
                if (error)
                {
                    UIController.getInstance().getComponent(cc.Widget).bottom = 0;
                }
                else
                {
                    loadedBanner = true;
                    UIController.getInstance().getComponent(cc.Widget).bottom = size.height / 13;
                }
            });
        }
    }
}
