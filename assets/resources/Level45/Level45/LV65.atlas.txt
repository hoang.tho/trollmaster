
LV65.png
size: 1480,368
format: RGBA8888
filter: Linear,Linear
repeat: none
1
  rotate: false
  xy: 1421, 225
  size: 41, 46
  orig: 41, 46
  offset: 0, 0
  index: -1
10
  rotate: false
  xy: 1441, 26
  size: 30, 41
  orig: 30, 41
  offset: 0, 0
  index: -1
11
  rotate: true
  xy: 1071, 131
  size: 124, 118
  orig: 124, 118
  offset: 0, 0
  index: -1
12
  rotate: true
  xy: 1112, 41
  size: 88, 54
  orig: 88, 54
  offset: 0, 0
  index: -1
13
  rotate: false
  xy: 919, 215
  size: 150, 151
  orig: 150, 151
  offset: 0, 0
  index: -1
14
  rotate: false
  xy: 86, 135
  size: 91, 24
  orig: 91, 24
  offset: 0, 0
  index: -1
15
  rotate: true
  xy: 1421, 147
  size: 76, 56
  orig: 76, 56
  offset: 0, 0
  index: -1
16
  rotate: false
  xy: 1214, 192
  size: 125, 142
  orig: 125, 142
  offset: 0, 0
  index: -1
17
  rotate: false
  xy: 1421, 273
  size: 40, 41
  orig: 40, 41
  offset: 0, 0
  index: -1
18
  rotate: true
  xy: 1417, 69
  size: 76, 59
  orig: 76, 59
  offset: 0, 0
  index: -1
19
  rotate: false
  xy: 2, 160
  size: 558, 206
  orig: 558, 206
  offset: 0, 0
  index: -1
2
  rotate: true
  xy: 862, 44
  size: 85, 248
  orig: 85, 248
  offset: 0, 0
  index: -1
20
  rotate: false
  xy: 2, 132
  size: 82, 26
  orig: 82, 26
  offset: 0, 0
  index: -1
21
  rotate: false
  xy: 179, 132
  size: 25, 26
  orig: 25, 26
  offset: 0, 0
  index: -1
22
  rotate: false
  xy: 862, 12
  size: 29, 30
  orig: 29, 30
  offset: 0, 0
  index: -1
23
  rotate: true
  xy: 1334, 85
  size: 125, 81
  orig: 125, 81
  offset: 0, 0
  index: -1
24
  rotate: true
  xy: 1247, 69
  size: 121, 85
  orig: 121, 85
  offset: 0, 0
  index: -1
3
  rotate: true
  xy: 1191, 20
  size: 235, 54
  orig: 235, 54
  offset: 0, 0
  index: -1
4
  rotate: false
  xy: 1247, 15
  size: 192, 52
  orig: 192, 52
  offset: 0, 0
  index: -1
5
  rotate: false
  xy: 1071, 257
  size: 141, 102
  orig: 141, 102
  offset: 0, 0
  index: -1
6
  rotate: false
  xy: 2, 2
  size: 558, 127
  orig: 558, 127
  offset: 0, 0
  index: -1
7
  rotate: true
  xy: 562, 131
  size: 235, 355
  orig: 235, 355
  offset: 0, 0
  index: -1
8
  rotate: false
  xy: 562, 10
  size: 298, 119
  orig: 298, 119
  offset: 0, 0
  index: -1
9
  rotate: false
  xy: 893, 10
  size: 21, 32
  orig: 21, 32
  offset: 0, 0
  index: -1
toc
  rotate: true
  xy: 1341, 212
  size: 113, 78
  orig: 113, 78
  offset: 0, 0
  index: -1
