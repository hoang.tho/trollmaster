
LV01.png
size: 2008,560
format: RGBA8888
filter: Linear,Linear
repeat: none
Layer1
  rotate: 270
  xy: 723, 11
  size: 231, 260
  orig: 231, 260
  offset: 0, 0
  index: -1
Layer10
  rotate: false
  xy: 1156, 26
  size: 71, 73
  orig: 71, 73
  offset: 0, 0
  index: -1
Layer11
  rotate: false
  xy: 56, 511
  size: 45, 47
  orig: 45, 47
  offset: 0, 0
  index: -1
Layer12
  rotate: false
  xy: 1292, 18
  size: 171, 187
  orig: 171, 187
  offset: 0, 0
  index: -1
Layer13
  rotate: false
  xy: 1156, 101
  size: 75, 78
  orig: 75, 78
  offset: 0, 0
  index: -1
Layer14
  rotate: false
  xy: 693, 14
  size: 47, 51
  orig: 47, 51
  offset: 0, 0
  index: -1
Layer15
  rotate: false
  xy: 1627, 43
  size: 196, 80
  orig: 196, 80
  offset: 0, 0
  index: -1
Layer16
  rotate: false
  xy: 1627, 125
  size: 211, 93
  orig: 211, 93
  offset: 0, 0
  index: -1
Layer17
  rotate: false
  xy: 985, 16
  size: 169, 163
  orig: 169, 163
  offset: 0, 0
  index: -1
Layer18
  rotate: false
  xy: 201, 428
  size: 99, 130
  orig: 99, 130
  offset: 0, 0
  index: -1
Layer19
  rotate: false
  xy: 1912, 3
  size: 34, 63
  orig: 34, 63
  offset: 0, 0
  index: -1
Layer2
  rotate: 180
  xy: 1738, 3
  size: 192, 137
  orig: 193, 137
  offset: 1, 0
  index: -1
Layer20
  rotate: false
  xy: 2, 391
  size: 45, 76
  orig: 45, 76
  offset: 0, 0
  index: -1
Layer21
  rotate: false
  xy: 252, 18
  size: 199, 47
  orig: 199, 47
  offset: 0, 0
  index: -1
Layer23
  rotate: false
  xy: 2, 24
  size: 248, 163
  orig: 248, 163
  offset: 0, 0
  index: -1
Layer24
  rotate: false
  xy: 521, 456
  size: 71, 102
  orig: 71, 102
  offset: 0, 0
  index: -1
Layer25
  rotate: true
  xy: 1903, 138
  size: 134, 102
  orig: 134, 103
  offset: 0, 1
  index: -1
Layer26
  rotate: false
  xy: 2, 181
  size: 833, 379
  orig: 833, 379
  offset: 0, 0
  index: -1
Layer27
  rotate: false
  xy: 252, 67
  size: 333, 112
  orig: 333, 112
  offset: 0, 0
  index: -1
Layer28
  rotate: 180
  xy: 772, 181
  size: 833, 379
  orig: 833, 379
  offset: 0, 0
  index: -1
Layer3
  rotate: 180
  xy: 1831, 20
  size: 86, 233
  orig: 86, 233
  offset: 0, 0
  index: -1
Layer4
  rotate: false
  xy: 1229, 7
  size: 37, 92
  orig: 37, 92
  offset: 0, 0
  index: -1
Layer5
  rotate: false
  xy: 2, 469
  size: 52, 89
  orig: 52, 89
  offset: 0, 0
  index: -1
Layer6
  rotate: false
  xy: 587, 38
  size: 104, 69
  orig: 104, 69
  offset: 0, 0
  index: -1
Layer7
  rotate: false
  xy: 453, 11
  size: 209, 54
  orig: 210, 54
  offset: 0, 0
  index: -1
Layer8
  rotate: true
  xy: 1605, 220
  size: 338, 296
  orig: 338, 296
  offset: 0, 0
  index: -1
Layer9
  rotate: false
  xy: 1465, 44
  size: 160, 174
  orig: 160, 174
  offset: 0, 0
  index: -1
toc
  rotate: false
  xy: 587, 109
  size: 113, 78
  orig: 113, 78
  offset: 0, 0
  index: -1
