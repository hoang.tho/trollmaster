
LV38.png
size: 2044,1720
format: RGBA8888
filter: Linear,Linear
repeat: none
Layer1
  rotate: false
  xy: 1432, 725
  size: 389, 464
  orig: 389, 464
  offset: 0, 0
  index: -1
Layer10
  rotate: true
  xy: 1759, 64
  size: 82, 154
  orig: 82, 154
  offset: 0, 0
  index: -1
Layer11
  rotate: false
  xy: 1307, 225
  size: 94, 43
  orig: 94, 43
  offset: 0, 0
  index: -1
Layer12
  rotate: false
  xy: 395, 163
  size: 94, 43
  orig: 94, 43
  offset: 0, 0
  index: -1
Layer13
  rotate: true
  xy: 1119, 359
  size: 269, 294
  orig: 269, 294
  offset: 0, 0
  index: -1
Layer14
  rotate: false
  xy: 417, 658
  size: 284, 158
  orig: 284, 158
  offset: 0, 0
  index: -1
Layer15
  rotate: false
  xy: 417, 633
  size: 151, 23
  orig: 151, 23
  offset: 0, 0
  index: -1
Layer19
  rotate: false
  xy: 417, 633
  size: 151, 23
  orig: 151, 23
  offset: 0, 0
  index: -1
Layer16
  rotate: true
  xy: 2019, 1356
  size: 91, 20
  orig: 91, 20
  offset: 0, 0
  index: -1
Layer17
  rotate: false
  xy: 776, 94
  size: 207, 66
  orig: 207, 66
  offset: 0, 0
  index: -1
Layer18
  rotate: false
  xy: 1021, 199
  size: 284, 158
  orig: 284, 158
  offset: 0, 0
  index: -1
Layer2
  rotate: true
  xy: 736, 630
  size: 392, 694
  orig: 392, 694
  offset: 0, 0
  index: -1
Layer20
  rotate: false
  xy: 570, 635
  size: 113, 21
  orig: 113, 21
  offset: 0, 0
  index: -1
Layer21
  rotate: false
  xy: 2, 381
  size: 126, 28
  orig: 126, 28
  offset: 0, 0
  index: -1
Layer22
  rotate: true
  xy: 1402, 1191
  size: 527, 359
  orig: 527, 359
  offset: 0, 0
  index: -1
Layer23
  rotate: false
  xy: 1432, 431
  size: 521, 292
  orig: 521, 292
  offset: 0, 0
  index: -1
Layer24
  rotate: false
  xy: 1994, 1639
  size: 39, 79
  orig: 39, 79
  offset: 0, 0
  index: -1
Layer25
  rotate: false
  xy: 1994, 1577
  size: 40, 60
  orig: 40, 60
  offset: 0, 0
  index: -1
Layer26
  rotate: false
  xy: 2, 818
  size: 732, 900
  orig: 732, 900
  offset: 0, 0
  index: -1
Layer27
  rotate: false
  xy: 1763, 1295
  size: 55, 74
  orig: 55, 74
  offset: 0, 0
  index: -1
Layer28
  rotate: false
  xy: 1957, 1237
  size: 60, 132
  orig: 60, 132
  offset: 0, 0
  index: -1
Layer29
  rotate: true
  xy: 698, 17
  size: 75, 115
  orig: 75, 115
  offset: 0, 0
  index: -1
Layer3
  rotate: false
  xy: 736, 1024
  size: 664, 694
  orig: 664, 694
  offset: 0, 0
  index: -1
Layer30
  rotate: false
  xy: 1307, 270
  size: 93, 87
  orig: 93, 87
  offset: 0, 0
  index: -1
Layer31
  rotate: false
  xy: 2, 16
  size: 202, 191
  orig: 211, 324
  offset: 0, 0
  index: -1
Layer32
  rotate: true
  xy: 776, 162
  size: 207, 243
  orig: 207, 243
  offset: 0, 0
  index: -1
Layer33
  rotate: false
  xy: 1763, 1371
  size: 229, 347
  orig: 229, 347
  offset: 0, 0
  index: -1
Layer34
  rotate: false
  xy: 1393, 36
  size: 166, 50
  orig: 166, 50
  offset: 0, 0
  index: -1
Layer35
  rotate: 270
  xy: 1446, 2
  size: 84, 134
  orig: 84, 134
  offset: 0, 0
  index: -1
Layer36
  rotate: false
  xy: 1957, 1151
  size: 67, 84
  orig: 67, 84
  offset: 0, 0
  index: -1
Layer37
  rotate: false
  xy: 1711, 148
  size: 281, 281
  orig: 281, 281
  offset: 0, 0
  index: -1
Layer38
  rotate: false
  xy: 493, 107
  size: 281, 262
  orig: 281, 262
  offset: 0, 0
  index: -1
Layer39
  rotate: false
  xy: 1393, 88
  size: 193, 70
  orig: 193, 70
  offset: 0, 0
  index: -1
Layer4
  rotate: false
  xy: 2, 208
  size: 489, 161
  orig: 489, 161
  offset: 0, 0
  index: -1
Layer40
  rotate: false
  xy: 1021, 40
  size: 122, 157
  orig: 122, 157
  offset: 0, 0
  index: -1
Layer41
  rotate: true
  xy: 1973, 1102
  size: 47, 66
  orig: 47, 66
  offset: 0, 0
  index: -1
Layer46
  rotate: true
  xy: 1973, 1102
  size: 47, 66
  orig: 47, 66
  offset: 0, 0
  index: -1
Layer42
  rotate: true
  xy: 561, 38
  size: 67, 135
  orig: 67, 135
  offset: 0, 0
  index: -1
Layer47
  rotate: true
  xy: 561, 38
  size: 67, 135
  orig: 67, 135
  offset: 0, 0
  index: -1
Layer43
  rotate: false
  xy: 1994, 1513
  size: 31, 62
  orig: 31, 62
  offset: 0, 0
  index: -1
Layer48
  rotate: false
  xy: 1994, 1513
  size: 31, 62
  orig: 31, 62
  offset: 0, 0
  index: -1
Layer44
  rotate: true
  xy: 1823, 1131
  size: 238, 132
  orig: 238, 132
  offset: 0, 0
  index: -1
Layer45
  rotate: false
  xy: 1145, 40
  size: 122, 157
  orig: 122, 157
  offset: 0, 0
  index: -1
Layer49
  rotate: false
  xy: 203, 94
  size: 190, 112
  orig: 190, 112
  offset: 0, 0
  index: -1
Layer5
  rotate: true
  xy: 417, 371
  size: 257, 349
  orig: 257, 349
  offset: 0, 0
  index: -1
Layer50
  rotate: false
  xy: 1269, 40
  size: 122, 157
  orig: 122, 157
  offset: 0, 0
  index: -1
Layer51
  rotate: true
  xy: 1973, 1053
  size: 47, 66
  orig: 47, 66
  offset: 0, 0
  index: -1
Layer52
  rotate: true
  xy: 205, 25
  size: 67, 135
  orig: 67, 135
  offset: 0, 0
  index: -1
Layer53
  rotate: true
  xy: 815, 32
  size: 60, 126
  orig: 60, 126
  offset: 0, 0
  index: -1
Layer54
  rotate: false
  xy: 1994, 1449
  size: 31, 62
  orig: 31, 62
  offset: 0, 0
  index: -1
Layer55
  rotate: false
  xy: 395, 50
  size: 164, 55
  orig: 164, 55
  offset: 0, 0
  index: -1
Layer56
  rotate: true
  xy: 1823, 938
  size: 191, 148
  orig: 191, 148
  offset: 0, 0
  index: -1
Layer6
  rotate: true
  xy: 768, 371
  size: 257, 349
  orig: 257, 349
  offset: 0, 0
  index: -1
Layer7
  rotate: true
  xy: 1415, 160
  size: 269, 294
  orig: 269, 294
  offset: 0, 0
  index: -1
Layer8
  rotate: true
  xy: 1823, 730
  size: 206, 132
  orig: 206, 132
  offset: 0, 0
  index: -1
Layer9
  rotate: true
  xy: 1588, 66
  size: 80, 169
  orig: 80, 169
  offset: 0, 0
  index: -1
no
  rotate: true
  xy: 2, 411
  size: 405, 413
  orig: 405, 413
  offset: 0, 0
  index: -1
toc
  rotate: false
  xy: 1915, 68
  size: 113, 78
  orig: 113, 78
  offset: 0, 0
  index: -1
