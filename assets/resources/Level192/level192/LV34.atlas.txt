
LV34.png
size: 768,240
format: RGBA8888
filter: Linear,Linear
repeat: none
1
  rotate: false
  xy: 2, 42
  size: 255, 64
  orig: 255, 64
  offset: 0, 0
  index: -1
10
  rotate: true
  xy: 395, 94
  size: 120, 78
  orig: 120, 78
  offset: 0, 0
  index: -1
11
  rotate: true
  xy: 604, 112
  size: 124, 44
  orig: 124, 44
  offset: 0, 0
  index: -1
12
  rotate: false
  xy: 232, 94
  size: 161, 142
  orig: 161, 142
  offset: 0, 0
  index: -1
13
  rotate: false
  xy: 2, 110
  size: 228, 126
  orig: 228, 126
  offset: 0, 0
  index: -1
14
  rotate: true
  xy: 449, 4
  size: 50, 72
  orig: 50, 72
  offset: 0, 0
  index: -1
15
  rotate: true
  xy: 650, 160
  size: 45, 36
  orig: 45, 36
  offset: 0, 0
  index: -1
16
  rotate: true
  xy: 688, 160
  size: 45, 36
  orig: 45, 36
  offset: 0, 0
  index: -1
17
  rotate: false
  xy: 664, 29
  size: 24, 81
  orig: 24, 81
  offset: 0, 0
  index: -1
18
  rotate: 180
  xy: 458, 28
  size: 94, 127
  orig: 94, 127
  offset: 0, 0
  index: -1
19
  rotate: false
  xy: 414, 24
  size: 33, 68
  orig: 33, 68
  offset: 0, 0
  index: -1
2
  rotate: true
  xy: 690, 58
  size: 52, 30
  orig: 52, 30
  offset: 0, 0
  index: -1
20
  rotate: false
  xy: 726, 136
  size: 37, 38
  orig: 37, 38
  offset: 0, 0
  index: -1
21
  rotate: 270
  xy: 7, 4
  size: 36, 280
  orig: 36, 281
  offset: 0, 1
  index: -1
22
  rotate: true
  xy: 650, 112
  size: 46, 43
  orig: 46, 43
  offset: 0, 0
  index: -1
23
  rotate: 180
  xy: 651, 9
  size: 96, 127
  orig: 96, 127
  offset: 0, 0
  index: -1
24
  rotate: false
  xy: 731, 12
  size: 29, 40
  orig: 29, 40
  offset: 0, 0
  index: -1
3
  rotate: true
  xy: 549, 85
  size: 151, 53
  orig: 151, 53
  offset: 0, 0
  index: -1
4
  rotate: true
  xy: 633, 38
  size: 72, 29
  orig: 72, 29
  offset: 0, 0
  index: -1
5
  rotate: false
  xy: 650, 207
  size: 58, 25
  orig: 58, 25
  offset: 0, 0
  index: -1
6
  rotate: false
  xy: 258, 26
  size: 154, 66
  orig: 154, 66
  offset: 0, 0
  index: -1
7
  rotate: false
  xy: 2, 28
  size: 16, 27
  orig: 16, 27
  offset: 0, 0
  index: -1
8
  rotate: false
  xy: 523, 3
  size: 16, 24
  orig: 16, 24
  offset: 0, 0
  index: -1
9
  rotate: false
  xy: 552, 27
  size: 79, 30
  orig: 79, 30
  offset: 0, 0
  index: -1
