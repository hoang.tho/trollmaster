
LV39.png
size: 1052,848
format: RGBA8888
filter: Linear,Linear
repeat: none
Layer1
  rotate: true
  xy: 478, 158
  size: 687, 288
  orig: 687, 288
  offset: 0, 0
  index: -1
Layer10
  rotate: false
  xy: 812, 7
  size: 162, 42
  orig: 164, 44
  offset: 1, 1
  index: -1
Layer11
  rotate: false
  xy: 481, 4
  size: 56, 25
  orig: 56, 25
  offset: 0, 0
  index: -1
Layer12
  rotate: false
  xy: 50, 11
  size: 53, 28
  orig: 53, 28
  offset: 0, 0
  index: -1
Layer13
  rotate: true
  xy: 1016, 372
  size: 118, 30
  orig: 118, 32
  offset: 0, 1
  index: -1
Layer14
  rotate: false
  xy: 344, 5
  size: 68, 53
  orig: 68, 53
  offset: 0, 0
  index: -1
Layer15
  rotate: false
  xy: 768, 639
  size: 201, 206
  orig: 201, 206
  offset: 0, 0
  index: -1
Layer17
  rotate: false
  xy: 884, 50
  size: 96, 54
  orig: 96, 54
  offset: 0, 0
  index: -1
Layer18
  rotate: false
  xy: 539, 3
  size: 46, 26
  orig: 46, 26
  offset: 0, 0
  index: -1
Layer19
  rotate: false
  xy: 344, 73
  size: 20, 13
  orig: 20, 13
  offset: 0, 0
  index: -1
Layer2
  rotate: false
  xy: 125, 58
  size: 137, 83
  orig: 137, 83
  offset: 0, 0
  index: -1
Layer20
  rotate: false
  xy: 2, 41
  size: 121, 100
  orig: 121, 100
  offset: 0, 0
  index: -1
Layer21
  rotate: false
  xy: 687, 4
  size: 62, 37
  orig: 62, 37
  offset: 0, 0
  index: -1
Layer22
  rotate: false
  xy: 344, 105
  size: 24, 15
  orig: 24, 15
  offset: 0, 0
  index: -1
Layer23
  rotate: false
  xy: 1014, 106
  size: 16, 139
  orig: 16, 139
  offset: 0, 0
  index: -1
Layer24
  rotate: true
  xy: 125, 3
  size: 53, 62
  orig: 53, 62
  offset: 0, 0
  index: -1
Layer25
  rotate: false
  xy: 380, 251
  size: 94, 115
  orig: 94, 115
  offset: 0, 0
  index: -1
Layer26
  rotate: false
  xy: 344, 122
  size: 23, 19
  orig: 23, 19
  offset: 0, 0
  index: -1
Layer27
  rotate: false
  xy: 694, 43
  size: 55, 113
  orig: 55, 113
  offset: 0, 0
  index: -1
Layer28
  rotate: false
  xy: 971, 639
  size: 43, 97
  orig: 43, 97
  offset: 0, 0
  index: -1
Layer29
  rotate: true
  xy: 2, 2
  size: 37, 46
  orig: 37, 46
  offset: 0, 0
  index: -1
Layer3
  rotate: false
  xy: 768, 106
  size: 201, 124
  orig: 201, 124
  offset: 0, 0
  index: -1
Layer30
  rotate: false
  xy: 380, 181
  size: 91, 68
  orig: 91, 68
  offset: 0, 0
  index: -1
Layer31
  rotate: false
  xy: 344, 88
  size: 23, 15
  orig: 23, 15
  offset: 0, 0
  index: -1
Layer32
  rotate: true
  xy: 189, 9
  size: 47, 68
  orig: 47, 68
  offset: 0, 0
  index: -1
Layer33
  rotate: true
  xy: 751, 3
  size: 38, 59
  orig: 38, 59
  offset: 0, 0
  index: -1
Layer34
  rotate: true
  xy: 751, 58
  size: 46, 131
  orig: 46, 131
  offset: 0, 0
  index: -1
Layer35
  rotate: true
  xy: 982, 38
  size: 66, 57
  orig: 66, 57
  offset: 0, 0
  index: -1
Layer36
  rotate: false
  xy: 414, 7
  size: 65, 47
  orig: 65, 47
  offset: 0, 0
  index: -1
Layer37
  rotate: true
  xy: 483, 31
  size: 30, 132
  orig: 30, 132
  offset: 0, 0
  index: -1
Layer38
  rotate: false
  xy: 971, 106
  size: 41, 110
  orig: 41, 110
  offset: 0, 0
  index: -1
Layer39
  rotate: false
  xy: 2, 143
  size: 376, 223
  orig: 376, 223
  offset: 0, 0
  index: -1
Layer4
  rotate: false
  xy: 2, 368
  size: 474, 477
  orig: 474, 477
  offset: 0, 0
  index: -1
Layer40
  rotate: false
  xy: 864, 248
  size: 80, 389
  orig: 80, 389
  offset: 0, 0
  index: -1
Layer41
  rotate: false
  xy: 768, 232
  size: 94, 405
  orig: 94, 405
  offset: 0, 0
  index: -1
Layer42
  rotate: false
  xy: 946, 249
  size: 66, 388
  orig: 66, 388
  offset: 0, 0
  index: -1
Layer43
  rotate: true
  xy: 971, 738
  size: 83, 49
  orig: 83, 49
  offset: 0, 0
  index: -1
Layer5
  rotate: false
  xy: 531, 68
  size: 161, 88
  orig: 161, 88
  offset: 0, 0
  index: -1
Layer6
  rotate: false
  xy: 617, 11
  size: 68, 55
  orig: 68, 55
  offset: 0, 0
  index: -1
Layer7
  rotate: true
  xy: 1014, 248
  size: 123, 35
  orig: 124, 38
  offset: 0, 2
  index: -1
Layer8
  rotate: false
  xy: 880, 3
  size: 147, 33
  orig: 148, 35
  offset: 0, 1
  index: -1
Layer9
  rotate: 180
  xy: 373, 56
  size: 157, 124
  orig: 159, 126
  offset: 1, 1
  index: -1
toc
  rotate: true
  xy: 264, 28
  size: 113, 78
  orig: 113, 78
  offset: 0, 0
  index: -1
