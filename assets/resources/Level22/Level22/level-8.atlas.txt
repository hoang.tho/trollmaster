
level-8.png
size: 634,523
format: RGBA8888
filter: Linear,Linear
repeat: none
Layer 1
  rotate: true
  xy: 573, 43
  size: 13, 11
  orig: 15, 13
  offset: 1, 1
  index: -1
Wardrobe_bodie
  rotate: false
  xy: 2, 188
  size: 252, 333
  orig: 254, 335
  offset: 1, 1
  index: -1
body
  rotate: false
  xy: 2, 12
  size: 135, 174
  orig: 135, 174
  offset: 0, 0
  index: -1
eye-1-fall
  rotate: false
  xy: 506, 12
  size: 65, 69
  orig: 67, 71
  offset: 1, 1
  index: -1
eye-2 copy
  rotate: true
  xy: 433, 275
  size: 31, 38
  orig: 33, 40
  offset: 1, 1
  index: -1
eye-2 copy 2
  rotate: true
  xy: 433, 242
  size: 31, 38
  orig: 33, 40
  offset: 1, 1
  index: -1
eye-2-fall
  rotate: false
  xy: 573, 160
  size: 57, 64
  orig: 59, 66
  offset: 1, 1
  index: -1
face
  rotate: true
  xy: 139, 13
  size: 173, 158
  orig: 175, 160
  offset: 1, 1
  index: -1
hair
  rotate: true
  xy: 413, 364
  size: 157, 65
  orig: 159, 67
  offset: 1, 1
  index: -1
hand-1a
  rotate: true
  xy: 334, 9
  size: 92, 86
  orig: 94, 93
  offset: 1, 6
  index: -1
hand-1b
  rotate: false
  xy: 480, 350
  size: 104, 68
  orig: 104, 68
  offset: 0, 0
  index: -1
hand-2a
  rotate: true
  xy: 471, 83
  size: 89, 96
  orig: 91, 105
  offset: 1, 1
  index: -1
hand-2b
  rotate: true
  xy: 590, 365
  size: 107, 39
  orig: 107, 41
  offset: 0, 2
  index: -1
iris-1
  rotate: true
  xy: 16, 2
  size: 8, 10
  orig: 10, 12
  offset: 1, 1
  index: -1
iris-1-fall
  rotate: true
  xy: 569, 83
  size: 10, 14
  orig: 12, 18
  offset: 1, 2
  index: -1
iris-2
  rotate: true
  xy: 28, 2
  size: 8, 9
  orig: 10, 11
  offset: 1, 1
  index: -1
iris-2-fall
  rotate: true
  xy: 2, 2
  size: 8, 12
  orig: 10, 14
  offset: 1, 1
  index: -1
leg-1a
  rotate: false
  xy: 479, 270
  size: 99, 78
  orig: 99, 79
  offset: 0, 1
  index: -1
leg-1a copy
  rotate: false
  xy: 407, 135
  size: 62, 49
  orig: 62, 49
  offset: 0, 0
  index: -1
leg-1b
  rotate: false
  xy: 586, 274
  size: 46, 89
  orig: 46, 89
  offset: 0, 0
  index: -1
lid-1-fall
  rotate: false
  xy: 569, 145
  size: 54, 13
  orig: 56, 15
  offset: 1, 1
  index: -1
lid-2-fall
  rotate: false
  xy: 433, 347
  size: 44, 15
  orig: 46, 17
  offset: 1, 1
  index: -1
money-1
  rotate: false
  xy: 334, 103
  size: 71, 26
  orig: 73, 28
  offset: 1, 1
  index: -1
money-2
  rotate: true
  xy: 422, 4
  size: 71, 26
  orig: 73, 28
  offset: 1, 1
  index: -1
money-3
  rotate: false
  xy: 334, 160
  size: 71, 27
  orig: 73, 29
  offset: 1, 1
  index: -1
money-4
  rotate: true
  xy: 450, 4
  size: 71, 26
  orig: 73, 28
  offset: 1, 1
  index: -1
money-5
  rotate: true
  xy: 478, 10
  size: 71, 26
  orig: 73, 28
  offset: 1, 1
  index: -1
money-6
  rotate: false
  xy: 334, 131
  size: 71, 27
  orig: 73, 29
  offset: 1, 1
  index: -1
mouth-fall
  rotate: false
  xy: 320, 189
  size: 88, 60
  orig: 90, 62
  offset: 1, 1
  index: -1
pink-body
  rotate: false
  xy: 307, 251
  size: 124, 111
  orig: 124, 111
  offset: 0, 0
  index: -1
pink-broom
  rotate: false
  xy: 480, 420
  size: 108, 101
  orig: 110, 103
  offset: 1, 1
  index: -1
pink-broom-cut
  rotate: false
  xy: 256, 189
  size: 62, 58
  orig: 68, 60
  offset: 5, 1
  index: -1
pink-face
  rotate: false
  xy: 473, 174
  size: 98, 94
  orig: 100, 96
  offset: 1, 1
  index: -1
pink-hair
  rotate: true
  xy: 307, 364
  size: 157, 104
  orig: 159, 112
  offset: 1, 7
  index: -1
pink-hand-1a
  rotate: true
  xy: 410, 186
  size: 54, 61
  orig: 54, 61
  offset: 0, 0
  index: -1
pink-hand-1b
  rotate: true
  xy: 585, 58
  size: 35, 37
  orig: 37, 39
  offset: 1, 1
  index: -1
pink-hand-2a
  rotate: true
  xy: 573, 95
  size: 48, 50
  orig: 50, 52
  offset: 1, 1
  index: -1
pink-hand-2b-angry
  rotate: false
  xy: 433, 308
  size: 39, 37
  orig: 41, 39
  offset: 1, 1
  index: -1
pink-hand-2c
  rotate: true
  xy: 422, 77
  size: 30, 46
  orig: 30, 46
  offset: 0, 0
  index: -1
pink-leg-1a
  rotate: true
  xy: 407, 109
  size: 24, 54
  orig: 24, 54
  offset: 0, 0
  index: -1
pink-leg-1c
  rotate: false
  xy: 580, 226
  size: 51, 46
  orig: 53, 48
  offset: 0, 2
  index: -1
pink-leg-2cd
  rotate: false
  xy: 590, 474
  size: 41, 47
  orig: 41, 47
  offset: 0, 0
  index: -1
pink-shadow
  rotate: true
  xy: 299, 2
  size: 185, 33
  orig: 187, 35
  offset: 1, 1
  index: -1
shadow
  rotate: true
  xy: 256, 249
  size: 272, 49
  orig: 274, 51
  offset: 1, 1
  index: -1
