
level33.png
size: 420,964
format: RGBA8888
filter: Linear,Linear
repeat: none
banchan1
  rotate: false
  xy: 265, 5
  size: 53, 24
  orig: 53, 24
  offset: 0, 0
  index: -1
banchan2
  rotate: false
  xy: 320, 31
  size: 50, 24
  orig: 50, 24
  offset: 0, 0
  index: -1
bantay1
  rotate: false
  xy: 49, 6
  size: 52, 49
  orig: 54, 51
  offset: 1, 1
  index: -1
body
  rotate: true
  xy: 138, 3
  size: 66, 126
  orig: 66, 126
  offset: 0, 0
  index: -1
canhtay1
  rotate: 270
  xy: 354, 371
  size: 68, 62
  orig: 68, 62
  offset: 0, 0
  index: -1
canhtay2
  rotate: false
  xy: 342, 5
  size: 68, 62
  orig: 69, 62
  offset: 0, 0
  index: -1
chan1
  rotate: false
  xy: 385, 702
  size: 27, 81
  orig: 29, 82
  offset: 1, 1
  index: -1
chan2
  rotate: false
  xy: 383, 619
  size: 27, 82
  orig: 28, 82
  offset: 0, 0
  index: -1
dau
  rotate: true
  xy: 228, 370
  size: 173, 172
  orig: 173, 172
  offset: 0, 0
  index: -1
img2/banchai
  rotate: true
  xy: 2, 67
  size: 45, 191
  orig: 45, 191
  offset: 0, 0
  index: -1
img2/banchai1
  rotate: false
  xy: 383, 514
  size: 26, 103
  orig: 26, 103
  offset: 0, 0
  index: -1
img2/bonruatay
  rotate: false
  xy: 2, 342
  size: 224, 201
  orig: 224, 201
  offset: 0, 0
  index: -1
img2/gangtay
  rotate: false
  xy: 147, 114
  size: 152, 113
  orig: 152, 113
  offset: 0, 0
  index: -1
img2/guong
  rotate: false
  xy: 2, 545
  size: 379, 416
  orig: 379, 416
  offset: 0, 0
  index: -1
img2/mat13
  rotate: false
  xy: 2, 198
  size: 143, 142
  orig: 143, 142
  offset: 0, 0
  index: -1
img2/mat15
  rotate: false
  xy: 228, 229
  size: 163, 139
  orig: 163, 139
  offset: 0, 0
  index: -1
img2/mat2
  rotate: true
  xy: 301, 110
  size: 117, 93
  orig: 117, 93
  offset: 0, 0
  index: -1
img2/mieng11
  rotate: false
  xy: 2, 19
  size: 46, 46
  orig: 46, 46
  offset: 0, 0
  index: -1
shadow2
  rotate: false
  xy: 195, 71
  size: 221, 37
  orig: 221, 37
  offset: 0, 0
  index: -1
toc
  rotate: false
  xy: 2, 118
  size: 113, 78
  orig: 113, 78
  offset: 0, 0
  index: -1
