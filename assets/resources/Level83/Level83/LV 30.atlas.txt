
LV 30.png
size: 1372,496
format: RGBA8888
filter: Linear,Linear
repeat: none
1
  rotate: false
  xy: 1162, 30
  size: 134, 82
  orig: 134, 82
  offset: 0, 0
  index: -1
11
  rotate: false
  xy: 503, 55
  size: 239, 189
  orig: 239, 189
  offset: 0, 0
  index: -1
12
  rotate: false
  xy: 2, 179
  size: 503, 314
  orig: 503, 314
  offset: 0, 0
  index: -1
13
  rotate: true
  xy: 223, 4
  size: 71, 88
  orig: 71, 88
  offset: 0, 0
  index: -1
14
  rotate: false
  xy: 891, 235
  size: 149, 174
  orig: 149, 174
  offset: 0, 0
  index: -1
15
  rotate: false
  xy: 1059, 20
  size: 101, 213
  orig: 101, 213
  offset: 0, 0
  index: -1
16
  rotate: false
  xy: 519, 18
  size: 33, 35
  orig: 33, 35
  offset: 0, 0
  index: -1
17
  rotate: false
  xy: 95, 467
  size: 66, 26
  orig: 66, 26
  offset: 0, 0
  index: -1
18
  rotate: false
  xy: 44, 425
  size: 16, 23
  orig: 16, 23
  offset: 0, 0
  index: -1
19
  rotate: false
  xy: 912, 9
  size: 56, 52
  orig: 56, 52
  offset: 0, 0
  index: -1
2
  rotate: false
  xy: 179, 214
  size: 36, 47
  orig: 36, 47
  offset: 0, 0
  index: -1
20
  rotate: false
  xy: 2, 369
  size: 16, 23
  orig: 16, 23
  offset: 0, 0
  index: -1
21
  rotate: false
  xy: 2, 394
  size: 40, 54
  orig: 40, 54
  offset: 0, 0
  index: -1
22
  rotate: true
  xy: 970, 5
  size: 53, 78
  orig: 53, 78
  offset: 0, 0
  index: -1
23
  rotate: false
  xy: 2, 450
  size: 91, 43
  orig: 91, 43
  offset: 0, 0
  index: -1
24
  rotate: false
  xy: 1145, 365
  size: 123, 121
  orig: 123, 121
  offset: 0, 0
  index: -1
25
  rotate: false
  xy: 205, 205
  size: 29, 79
  orig: 29, 80
  offset: 0, 1
  index: -1
26
  rotate: false
  xy: 163, 477
  size: 41, 16
  orig: 41, 16
  offset: 0, 0
  index: -1
27
  rotate: false
  xy: 275, 308
  size: 38, 25
  orig: 38, 25
  offset: 0, 0
  index: -1
28
  rotate: true
  xy: 554, 4
  size: 49, 72
  orig: 49, 72
  offset: 0, 0
  index: -1
29
  rotate: true
  xy: 313, 3
  size: 72, 105
  orig: 72, 105
  offset: 0, 0
  index: -1
3
  rotate: false
  xy: 114, 179
  size: 108, 33
  orig: 108, 33
  offset: 0, 0
  index: -1
30
  rotate: false
  xy: 628, 8
  size: 76, 45
  orig: 76, 45
  offset: 0, 0
  index: -1
31
  rotate: false
  xy: 443, 456
  size: 32, 37
  orig: 32, 37
  offset: 0, 0
  index: -1
33
  rotate: false
  xy: 1270, 365
  size: 98, 85
  orig: 98, 85
  offset: 0, 0
  index: -1
34
  rotate: false
  xy: 1042, 235
  size: 101, 237
  orig: 101, 237
  offset: 0, 0
  index: -1
35
  rotate: false
  xy: 239, 77
  size: 262, 229
  orig: 262, 229
  offset: 0, 0
  index: -1
36
  rotate: false
  xy: 62, 432
  size: 11, 16
  orig: 11, 16
  offset: 0, 0
  index: -1
37
  rotate: false
  xy: 95, 449
  size: 11, 16
  orig: 11, 16
  offset: 0, 0
  index: -1
38
  rotate: false
  xy: 319, 308
  size: 135, 58
  orig: 135, 58
  offset: 0, 0
  index: -1
39
  rotate: false
  xy: 420, 18
  size: 97, 35
  orig: 97, 35
  offset: 0, 0
  index: -1
40
  rotate: true
  xy: 1145, 235
  size: 128, 126
  orig: 128, 126
  offset: 0, 0
  index: -1
41
  rotate: false
  xy: 2, 36
  size: 219, 141
  orig: 219, 141
  offset: 0, 0
  index: -1
42
  rotate: false
  xy: 1269, 114
  size: 100, 110
  orig: 100, 110
  offset: 0, 0
  index: -1
43
  rotate: false
  xy: 507, 246
  size: 236, 247
  orig: 236, 247
  offset: 0, 0
  index: -1
5
  rotate: false
  xy: 912, 63
  size: 145, 170
  orig: 145, 170
  offset: 0, 0
  index: -1
6
  rotate: false
  xy: 745, 235
  size: 144, 190
  orig: 144, 190
  offset: 0, 0
  index: -1
7
  rotate: false
  xy: 1162, 114
  size: 105, 119
  orig: 105, 119
  offset: 0, 0
  index: -1
8
  rotate: true
  xy: 744, 55
  size: 178, 166
  orig: 178, 166
  offset: 0, 0
  index: -1
9
  rotate: false
  xy: 1298, 22
  size: 71, 90
  orig: 71, 90
  offset: 0, 0
  index: -1
toc
  rotate: true
  xy: 1273, 226
  size: 113, 78
  orig: 113, 78
  offset: 0, 0
  index: -1
