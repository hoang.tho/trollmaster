
LV45.png
size: 2040,680
format: RGBA8888
filter: Linear,Linear
repeat: none
Layer1
  rotate: true
  xy: 1254, 4
  size: 98, 117
  orig: 100, 120
  offset: 0, 2
  index: -1
Layer10
  rotate: 180
  xy: 1543, 13
  size: 167, 97
  orig: 169, 101
  offset: 2, 2
  index: -1
Layer11
  rotate: false
  xy: 1113, 52
  size: 65, 62
  orig: 65, 62
  offset: 0, 0
  index: -1
Layer12
  rotate: false
  xy: 848, 339
  size: 77, 35
  orig: 77, 35
  offset: 0, 0
  index: -1
Layer13
  rotate: false
  xy: 1324, 157
  size: 315, 278
  orig: 315, 278
  offset: 0, 0
  index: -1
Layer14
  rotate: false
  xy: 1890, 94
  size: 124, 53
  orig: 124, 53
  offset: 0, 0
  index: -1
Layer15
  rotate: false
  xy: 1625, 26
  size: 154, 76
  orig: 156, 77
  offset: 1, 0
  index: -1
Layer16
  rotate: false
  xy: 1921, 299
  size: 115, 147
  orig: 115, 147
  offset: 0, 0
  index: -1
Layer17
  rotate: false
  xy: 1147, 116
  size: 165, 95
  orig: 165, 95
  offset: 0, 0
  index: -1
Layer18
  rotate: false
  xy: 476, 237
  size: 217, 137
  orig: 217, 137
  offset: 0, 0
  index: -1
Layer19
  rotate: false
  xy: 226, 51
  size: 36, 55
  orig: 36, 55
  offset: 0, 0
  index: -1
Layer2
  rotate: true
  xy: 1324, 437
  size: 238, 488
  orig: 238, 488
  offset: 0, 0
  index: -1
Layer20
  rotate: false
  xy: 1294, 288
  size: 27, 47
  orig: 27, 47
  offset: 0, 0
  index: -1
Layer21
  rotate: false
  xy: 615, 131
  size: 210, 58
  orig: 210, 58
  offset: 0, 0
  index: -1
Layer22
  rotate: false
  xy: 2, 126
  size: 218, 64
  orig: 218, 64
  offset: 0, 0
  index: -1
Layer23
  rotate: false
  xy: 623, 97
  size: 195, 32
  orig: 195, 32
  offset: 0, 0
  index: -1
Layer24
  rotate: false
  xy: 1641, 103
  size: 132, 108
  orig: 132, 108
  offset: 0, 0
  index: -1
Layer25
  rotate: true
  xy: 1814, 448
  size: 227, 142
  orig: 227, 142
  offset: 0, 0
  index: -1
Layer26
  rotate: false
  xy: 405, 157
  size: 208, 78
  orig: 208, 78
  offset: 0, 0
  index: -1
Layer27
  rotate: false
  xy: 439, 249
  size: 35, 58
  orig: 35, 58
  offset: 0, 0
  index: -1
Layer28
  rotate: true
  xy: 2, 202
  size: 29, 48
  orig: 29, 48
  offset: 0, 0
  index: -1
Layer29
  rotate: false
  xy: 2, 85
  size: 160, 39
  orig: 160, 39
  offset: 0, 0
  index: -1
Layer3
  rotate: true
  xy: 1775, 69
  size: 62, 66
  orig: 62, 66
  offset: 0, 0
  index: -1
Layer30
  rotate: true
  xy: 277, 81
  size: 63, 72
  orig: 63, 72
  offset: 0, 0
  index: -1
Layer31
  rotate: true
  xy: 1873, 42
  size: 51, 158
  orig: 53, 160
  offset: 0, 0
  index: -1
Layer32
  rotate: true
  xy: 1180, 55
  size: 59, 67
  orig: 59, 67
  offset: 0, 0
  index: -1
Layer33
  rotate: true
  xy: 913, 39
  size: 60, 155
  orig: 62, 157
  offset: 1, 2
  index: -1
Layer34
  rotate: false
  xy: 615, 200
  size: 66, 35
  orig: 66, 35
  offset: 0, 0
  index: -1
Layer35
  rotate: false
  xy: 1476, 59
  size: 65, 55
  orig: 65, 55
  offset: 0, 0
  index: -1
Layer36
  rotate: false
  xy: 623, 71
  size: 239, 24
  orig: 239, 24
  offset: 0, 0
  index: -1
Layer37
  rotate: false
  xy: 1958, 468
  size: 74, 70
  orig: 74, 70
  offset: 0, 0
  index: -1
Layer38
  rotate: false
  xy: 1578, 85
  size: 58, 70
  orig: 58, 70
  offset: 0, 0
  index: -1
Layer39
  rotate: false
  xy: 827, 110
  size: 211, 55
  orig: 211, 55
  offset: 0, 0
  index: -1
Layer4
  rotate: false
  xy: 227, 146
  size: 249, 232
  orig: 249, 232
  offset: 0, 0
  index: -1
Layer40
  rotate: true
  xy: 1147, 213
  size: 122, 145
  orig: 122, 145
  offset: 0, 0
  index: -1
Layer41
  rotate: false
  xy: 1958, 540
  size: 75, 135
  orig: 75, 135
  offset: 0, 0
  index: -1
Layer42
  rotate: false
  xy: 1113, 120
  size: 30, 61
  orig: 30, 61
  offset: 0, 0
  index: -1
Layer43
  rotate: false
  xy: 1843, 62
  size: 42, 69
  orig: 42, 69
  offset: 0, 0
  index: -1
Layer44
  rotate: true
  xy: 222, 108
  size: 124, 53
  orig: 124, 53
  offset: 0, 0
  index: -1
Layer447
  rotate: false
  xy: 1890, 165
  size: 28, 46
  orig: 28, 46
  offset: 0, 0
  index: -1
Layer45
  rotate: false
  xy: 1921, 149
  size: 113, 148
  orig: 113, 148
  offset: 0, 0
  index: -1
Layer46
  rotate: false
  xy: 695, 191
  size: 151, 183
  orig: 151, 183
  offset: 0, 0
  index: -1
Layer48
  rotate: false
  xy: 204, 353
  size: 16, 23
  orig: 16, 23
  offset: 0, 0
  index: -1
Layer49
  rotate: true
  xy: 369, 93
  size: 62, 126
  orig: 62, 126
  offset: 0, 0
  index: -1
Layer5
  rotate: false
  xy: 1040, 115
  size: 71, 66
  orig: 71, 66
  offset: 0, 0
  index: -1
Layer50
  rotate: false
  xy: 351, 36
  size: 34, 55
  orig: 34, 55
  offset: 0, 0
  index: -1
Layer51
  rotate: false
  xy: 21, 350
  size: 16, 26
  orig: 16, 26
  offset: 0, 0
  index: -1
Layer52
  rotate: false
  xy: 1005, 183
  size: 140, 152
  orig: 140, 152
  offset: 0, 0
  index: -1
Layer53
  rotate: false
  xy: 848, 167
  size: 155, 168
  orig: 155, 168
  offset: 0, 0
  index: -1
Layer54
  rotate: false
  xy: 387, 30
  size: 30, 61
  orig: 30, 61
  offset: 0, 0
  index: -1
Layer55
  rotate: false
  xy: 2, 334
  size: 17, 42
  orig: 17, 42
  offset: 0, 0
  index: -1
Layer56
  rotate: false
  xy: 864, 39
  size: 42, 69
  orig: 42, 69
  offset: 0, 0
  index: -1
Layer57
  rotate: true
  xy: 419, 63
  size: 28, 46
  orig: 28, 46
  offset: 0, 0
  index: -1
Layer58
  rotate: false
  xy: 164, 56
  size: 60, 50
  orig: 60, 50
  offset: 0, 0
  index: -1
Layer59
  rotate: false
  xy: 497, 76
  size: 124, 53
  orig: 124, 53
  offset: 0, 0
  index: -1
Layer6
  rotate: false
  xy: 937, 337
  size: 385, 338
  orig: 385, 338
  offset: 0, 0
  index: -1
Layer60
  rotate: false
  xy: 1314, 116
  size: 262, 39
  orig: 262, 39
  offset: 0, 0
  index: -1
Layer7
  rotate: 270
  xy: 925, 49
  size: 63, 186
  orig: 63, 186
  offset: 0, 0
  index: -1
Layer8
  rotate: true
  xy: 2, 192
  size: 185, 231
  orig: 185, 232
  offset: 0, 0
  index: -1
Layer9
  rotate: false
  xy: 1234, 26
  size: 241, 89
  orig: 242, 90
  offset: 0, 0
  index: -1
coxi1
  rotate: false
  xy: 487, 376
  size: 448, 299
  orig: 448, 299
  offset: 0, 0
  index: -1
coxi2
  rotate: false
  xy: 2, 378
  size: 483, 297
  orig: 483, 297
  offset: 0, 0
  index: -1
toc
  rotate: false
  xy: 1775, 133
  size: 113, 78
  orig: 113, 78
  offset: 0, 0
  index: -1
tocc
  rotate: false
  xy: 1641, 213
  size: 278, 222
  orig: 278, 222
  offset: 0, 0
  index: -1
