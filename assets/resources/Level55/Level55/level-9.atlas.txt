
level-9.png
size: 650,522
format: RGBA8888
filter: Linear,Linear
repeat: none
blush-push
  rotate: false
  xy: 413, 220
  size: 77, 18
  orig: 79, 20
  offset: 1, 1
  index: -1
body
  rotate: false
  xy: 555, 415
  size: 83, 105
  orig: 83, 105
  offset: 0, 0
  index: -1
brown-1
  rotate: true
  xy: 342, 380
  size: 20, 16
  orig: 22, 18
  offset: 1, 1
  index: -1
brown-2
  rotate: true
  xy: 475, 194
  size: 24, 13
  orig: 26, 15
  offset: 1, 1
  index: -1
car
  rotate: true
  xy: 2, 102
  size: 418, 245
  orig: 438, 247
  offset: 19, 1
  index: -1
eye-1
  rotate: false
  xy: 249, 338
  size: 41, 40
  orig: 43, 42
  offset: 1, 1
  index: -1
eye-1-push
  rotate: false
  xy: 362, 362
  size: 37, 38
  orig: 39, 42
  offset: 1, 3
  index: -1
eye-2
  rotate: false
  xy: 401, 363
  size: 35, 37
  orig: 37, 39
  offset: 1, 1
  index: -1
eye-2-push
  rotate: false
  xy: 438, 364
  size: 35, 36
  orig: 37, 39
  offset: 1, 2
  index: -1
face
  rotate: true
  xy: 554, 233
  size: 103, 94
  orig: 105, 96
  offset: 1, 1
  index: -1
hand-1a
  rotate: true
  xy: 249, 380
  size: 51, 52
  orig: 56, 54
  offset: 4, 1
  index: -1
hand-1b
  rotate: false
  xy: 376, 165
  size: 35, 63
  orig: 35, 63
  offset: 0, 0
  index: -1
hand-2a
  rotate: false
  xy: 560, 99
  size: 70, 64
  orig: 70, 65
  offset: 0, 1
  index: -1
hand-2a-push
  rotate: true
  xy: 249, 464
  size: 56, 53
  orig: 58, 55
  offset: 1, 1
  index: -1
hand-2b
  rotate: false
  xy: 559, 80
  size: 70, 17
  orig: 70, 18
  offset: 0, 1
  index: -1
hand-2b-push
  rotate: true
  xy: 632, 97
  size: 66, 16
  orig: 66, 17
  offset: 0, 1
  index: -1
heart
  rotate: false
  xy: 249, 102
  size: 19, 22
  orig: 21, 24
  offset: 1, 1
  index: -1
iris-1
  rotate: true
  xy: 539, 90
  size: 12, 18
  orig: 12, 18
  offset: 0, 0
  index: -1
iris-2
  rotate: true
  xy: 475, 362
  size: 12, 16
  orig: 12, 16
  offset: 0, 0
  index: -1
leg-1a
  rotate: false
  xy: 492, 280
  size: 60, 48
  orig: 60, 48
  offset: 0, 0
  index: -1
leg-2a
  rotate: false
  xy: 413, 171
  size: 60, 47
  orig: 60, 48
  offset: 0, 1
  index: -1
leg-2b
  rotate: true
  xy: 249, 433
  size: 29, 53
  orig: 29, 53
  offset: 0, 0
  index: -1
lid-1
  rotate: false
  xy: 303, 380
  size: 37, 20
  orig: 39, 22
  offset: 1, 1
  index: -1
lid-2
  rotate: false
  xy: 270, 106
  size: 34, 18
  orig: 39, 20
  offset: 4, 1
  index: -1
pink-angry
  rotate: true
  xy: 637, 401
  size: 12, 11
  orig: 16, 13
  offset: 1, 1
  index: -1
pink-body
  rotate: true
  xy: 382, 240
  size: 120, 108
  orig: 120, 108
  offset: 0, 0
  index: -1
pink-eye-1
  rotate: false
  xy: 529, 127
  size: 29, 36
  orig: 31, 38
  offset: 1, 1
  index: -1
pink-eye-2
  rotate: false
  xy: 497, 127
  size: 30, 36
  orig: 32, 38
  offset: 1, 1
  index: -1
pink-face
  rotate: true
  xy: 214, 5
  size: 95, 90
  orig: 97, 92
  offset: 1, 1
  index: -1
pink-hair
  rotate: false
  xy: 494, 165
  size: 152, 63
  orig: 154, 65
  offset: 1, 1
  index: -1
pink-hand-1a
  rotate: true
  xy: 292, 338
  size: 40, 33
  orig: 42, 35
  offset: 1, 1
  index: -1
pink-hand-1b
  rotate: false
  xy: 588, 52
  size: 42, 26
  orig: 44, 28
  offset: 1, 1
  index: -1
pink-hand-1c
  rotate: false
  xy: 362, 338
  size: 18, 21
  orig: 52, 28
  offset: 1, 1
  index: -1
pink-hand-2a
  rotate: true
  xy: 327, 338
  size: 40, 33
  orig: 42, 35
  offset: 1, 1
  index: -1
pink-hand-2b
  rotate: true
  xy: 588, 8
  size: 42, 26
  orig: 44, 28
  offset: 1, 1
  index: -1
pink-hand-2c
  rotate: false
  xy: 539, 104
  size: 18, 21
  orig: 52, 28
  offset: 33, 1
  index: -1
pink-iris-1
  rotate: false
  xy: 640, 493
  size: 7, 10
  orig: 9, 12
  offset: 1, 1
  index: -1
pink-iris-2
  rotate: false
  xy: 640, 482
  size: 7, 9
  orig: 9, 11
  offset: 1, 1
  index: -1
pink-leg-1a
  rotate: false
  xy: 492, 230
  size: 60, 48
  orig: 60, 48
  offset: 0, 0
  index: -1
pink-leg-1b
  rotate: true
  xy: 494, 330
  size: 29, 58
  orig: 29, 58
  offset: 0, 0
  index: -1
pink-leg-2a
  rotate: true
  xy: 539, 18
  size: 60, 47
  orig: 60, 47
  offset: 0, 0
  index: -1
pink-lid-1
  rotate: true
  xy: 475, 165
  size: 27, 12
  orig: 29, 14
  offset: 1, 1
  index: -1
pink-lid-2
  rotate: true
  xy: 475, 376
  size: 24, 17
  orig: 26, 19
  offset: 1, 1
  index: -1
pink-smile
  rotate: true
  xy: 640, 505
  size: 15, 8
  orig: 18, 10
  offset: 2, 1
  index: -1
smile-push
  rotate: true
  xy: 637, 386
  size: 13, 10
  orig: 15, 12
  offset: 1, 1
  index: -1
smile-pusha
  rotate: false
  xy: 494, 361
  size: 59, 39
  orig: 61, 41
  offset: 1, 1
  index: -1
splash-1
  rotate: true
  xy: 306, 402
  size: 118, 247
  orig: 120, 249
  offset: 1, 1
  index: -1
splash-2
  rotate: true
  xy: 376, 127
  size: 36, 119
  orig: 38, 121
  offset: 1, 1
  index: -1
splash-3
  rotate: false
  xy: 494, 8
  size: 43, 117
  orig: 45, 119
  offset: 1, 1
  index: -1
tire-1
  rotate: true
  xy: 2, 2
  size: 98, 104
  orig: 100, 106
  offset: 1, 1
  index: -1
tire-2
  rotate: true
  xy: 108, 3
  size: 97, 104
  orig: 99, 106
  offset: 1, 1
  index: -1
tire-3
  rotate: true
  xy: 555, 338
  size: 75, 80
  orig: 77, 82
  offset: 1, 1
  index: -1
water-1-cut
  rotate: false
  xy: 306, 2
  size: 186, 122
  orig: 186, 122
  offset: 0, 0
  index: -1
water-2
  rotate: true
  xy: 249, 126
  size: 210, 125
  orig: 212, 127
  offset: 1, 1
  index: -1
