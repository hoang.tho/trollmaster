
LV36.png
size: 2020,852
format: RGBA8888
filter: Linear,Linear
repeat: none
Layer1
  rotate: true
  xy: 1146, 13
  size: 87, 183
  orig: 89, 185
  offset: 1, 1
  index: -1
Layer10
  rotate: true
  xy: 661, 230
  size: 249, 287
  orig: 249, 287
  offset: 0, 0
  index: -1
Layer11
  rotate: 180
  xy: 1679, 3
  size: 232, 112
  orig: 232, 113
  offset: 0, 0
  index: -1
Layer12
  rotate: false
  xy: 950, 93
  size: 171, 149
  orig: 171, 149
  offset: 0, 0
  index: -1
Layer13
  rotate: true
  xy: 1423, 488
  size: 361, 312
  orig: 361, 312
  offset: 0, 0
  index: -1
Layer14
  rotate: false
  xy: 1997, 800
  size: 21, 49
  orig: 21, 49
  offset: 0, 0
  index: -1
Layer16
  rotate: true
  xy: 650, 111
  size: 117, 184
  orig: 117, 184
  offset: 0, 0
  index: -1
Layer17
  rotate: true
  xy: 1123, 100
  size: 142, 178
  orig: 142, 178
  offset: 0, 0
  index: -1
Layer18
  rotate: true
  xy: 254, 437
  size: 30, 54
  orig: 30, 54
  offset: 0, 0
  index: -1
Layer30
  rotate: true
  xy: 254, 437
  size: 30, 54
  orig: 30, 54
  offset: 0, 0
  index: -1
Layer19
  rotate: true
  xy: 1608, 144
  size: 39, 65
  orig: 39, 65
  offset: 0, 0
  index: -1
Layer25
  rotate: true
  xy: 1608, 144
  size: 39, 65
  orig: 39, 65
  offset: 0, 0
  index: -1
Layer31
  rotate: true
  xy: 1608, 144
  size: 39, 65
  orig: 39, 65
  offset: 0, 0
  index: -1
Layer2
  rotate: false
  xy: 836, 135
  size: 99, 93
  orig: 99, 93
  offset: 0, 0
  index: -1
Layer15
  rotate: false
  xy: 836, 135
  size: 99, 93
  orig: 99, 93
  offset: 0, 0
  index: -1
Layer20
  rotate: false
  xy: 2, 153
  size: 209, 74
  orig: 209, 74
  offset: 0, 0
  index: -1
Layer21
  rotate: false
  xy: 537, 56
  size: 217, 53
  orig: 217, 53
  offset: 0, 0
  index: -1
Layer22
  rotate: true
  xy: 1717, 99
  size: 135, 168
  orig: 135, 168
  offset: 0, 0
  index: -1
Layer23
  rotate: false
  xy: 1717, 239
  size: 163, 204
  orig: 163, 204
  offset: 0, 0
  index: -1
Layer24
  rotate: true
  xy: 254, 252
  size: 30, 54
  orig: 30, 54
  offset: 0, 0
  index: -1
Layer26
  rotate: false
  xy: 2, 96
  size: 233, 55
  orig: 233, 55
  offset: 0, 0
  index: -1
Layer27
  rotate: true
  xy: 545, 216
  size: 251, 103
  orig: 251, 103
  offset: 0, 0
  index: -1
Layer271
  rotate: false
  xy: 1094, 502
  size: 327, 347
  orig: 327, 347
  offset: 0, 0
  index: -1
Layer28
  rotate: true
  xy: 1303, 94
  size: 148, 139
  orig: 148, 139
  offset: 0, 0
  index: -1
Layer29
  rotate: false
  xy: 427, 100
  size: 108, 139
  orig: 108, 139
  offset: 0, 0
  index: -1
Layer3
  rotate: true
  xy: 537, 128
  size: 86, 105
  orig: 86, 105
  offset: 0, 0
  index: -1
Layer32
  rotate: false
  xy: 1444, 86
  size: 233, 55
  orig: 233, 55
  offset: 0, 0
  index: -1
Layer33
  rotate: false
  xy: 1444, 143
  size: 162, 83
  orig: 162, 83
  offset: 0, 0
  index: -1
Layer34
  rotate: false
  xy: 2, 469
  size: 727, 380
  orig: 727, 380
  offset: 0, 0
  index: -1
Layer35
  rotate: true
  xy: 1447, 228
  size: 258, 268
  orig: 258, 268
  offset: 0, 0
  index: -1
Layer36
  rotate: false
  xy: 1094, 244
  size: 351, 242
  orig: 351, 242
  offset: 0, 0
  index: -1
Layer37
  rotate: true
  xy: 1737, 445
  size: 404, 258
  orig: 404, 258
  offset: 0, 0
  index: -1
Layer38
  rotate: true
  xy: 2, 229
  size: 238, 250
  orig: 238, 250
  offset: 0, 0
  index: -1
Layer4
  rotate: true
  xy: 254, 93
  size: 147, 171
  orig: 147, 171
  offset: 0, 0
  index: -1
Layer5
  rotate: true
  xy: 873, 72
  size: 37, 274
  orig: 37, 274
  offset: 0, 0
  index: -1
Layer6
  rotate: true
  xy: 254, 241
  size: 228, 291
  orig: 228, 292
  offset: 0, 1
  index: -1
Layer7
  rotate: true
  xy: 193, 60
  size: 40, 263
  orig: 40, 264
  offset: 0, 1
  index: -1
Layer8
  rotate: false
  xy: 1608, 185
  size: 96, 41
  orig: 96, 41
  offset: 0, 0
  index: -1
Layer9
  rotate: false
  xy: 1913, 58
  size: 96, 41
  orig: 96, 41
  offset: 0, 0
  index: -1
dau_den
  rotate: false
  xy: 731, 481
  size: 361, 368
  orig: 361, 368
  offset: 0, 0
  index: -1
lo1
  rotate: true
  xy: 950, 272
  size: 207, 132
  orig: 207, 132
  offset: 0, 0
  index: -1
lo2
  rotate: true
  xy: 1882, 236
  size: 207, 132
  orig: 207, 132
  offset: 0, 0
  index: -1
lo3
  rotate: true
  xy: 1887, 101
  size: 133, 126
  orig: 133, 126
  offset: 0, 0
  index: -1
toc
  rotate: false
  xy: 756, 31
  size: 113, 78
  orig: 113, 78
  offset: 0, 0
  index: -1
