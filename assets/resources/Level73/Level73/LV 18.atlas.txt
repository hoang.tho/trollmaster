
LV 18.png
size: 1288,272
format: RGBA8888
filter: Linear,Linear
repeat: none
1
  rotate: false
  xy: 1260, 232
  size: 25, 29
  orig: 25, 29
  offset: 0, 0
  index: -1
10
  rotate: false
  xy: 602, 30
  size: 156, 65
  orig: 156, 65
  offset: 0, 0
  index: -1
11
  rotate: false
  xy: 2, 4
  size: 322, 265
  orig: 322, 265
  offset: 0, 0
  index: -1
12
  rotate: false
  xy: 1134, 5
  size: 83, 33
  orig: 83, 33
  offset: 0, 0
  index: -1
13
  rotate: false
  xy: 1263, 64
  size: 18, 29
  orig: 18, 29
  offset: 0, 0
  index: -1
14
  rotate: false
  xy: 1054, 65
  size: 68, 70
  orig: 68, 70
  offset: 0, 0
  index: -1
15
  rotate: false
  xy: 1263, 95
  size: 18, 26
  orig: 18, 26
  offset: 0, 0
  index: -1
16
  rotate: true
  xy: 970, 5
  size: 70, 82
  orig: 70, 82
  offset: 0, 0
  index: -1
17
  rotate: true
  xy: 1043, 137
  size: 132, 48
  orig: 132, 48
  offset: 0, 0
  index: -1
18
  rotate: true
  xy: 602, 97
  size: 172, 151
  orig: 172, 151
  offset: 0, 0
  index: -1
19
  rotate: false
  xy: 1093, 137
  size: 53, 86
  orig: 53, 86
  offset: 0, 0
  index: -1
2
  rotate: true
  xy: 835, 104
  size: 141, 60
  orig: 141, 60
  offset: 0, 0
  index: -1
20
  rotate: true
  xy: 1251, 18
  size: 44, 24
  orig: 44, 24
  offset: 0, 0
  index: -1
21
  rotate: true
  xy: 1243, 137
  size: 45, 24
  orig: 45, 24
  offset: 0, 0
  index: -1
22
  rotate: false
  xy: 1148, 137
  size: 56, 76
  orig: 56, 76
  offset: 0, 0
  index: -1
23
  rotate: true
  xy: 707, 3
  size: 25, 86
  orig: 25, 86
  offset: 0, 0
  index: -1
24
  rotate: true
  xy: 895, 8
  size: 94, 73
  orig: 94, 73
  offset: 0, 0
  index: -1
25
  rotate: true
  xy: 1054, 3
  size: 60, 78
  orig: 60, 78
  offset: 0, 0
  index: -1
26
  rotate: false
  xy: 1206, 137
  size: 35, 72
  orig: 35, 72
  offset: 0, 0
  index: -1
27
  rotate: false
  xy: 970, 77
  size: 71, 91
  orig: 71, 91
  offset: 0, 0
  index: -1
28
  rotate: false
  xy: 1219, 19
  size: 30, 43
  orig: 30, 43
  offset: 0, 0
  index: -1
29
  rotate: false
  xy: 1228, 211
  size: 30, 43
  orig: 30, 43
  offset: 0, 0
  index: -1
3
  rotate: true
  xy: 1260, 184
  size: 46, 22
  orig: 46, 22
  offset: 0, 0
  index: -1
30
  rotate: false
  xy: 326, 5
  size: 274, 264
  orig: 274, 264
  offset: 0, 0
  index: -1
31
  rotate: true
  xy: 760, 11
  size: 92, 134
  orig: 94, 135
  offset: 2, 1
  index: -1
4
  rotate: false
  xy: 1181, 71
  size: 48, 64
  orig: 48, 64
  offset: 0, 0
  index: -1
5
  rotate: true
  xy: 1148, 215
  size: 51, 78
  orig: 51, 78
  offset: 0, 0
  index: -1
6
  rotate: true
  xy: 1231, 64
  size: 71, 30
  orig: 71, 30
  offset: 0, 0
  index: -1
7
  rotate: true
  xy: 897, 170
  size: 99, 83
  orig: 99, 83
  offset: 0, 0
  index: -1
8
  rotate: false
  xy: 602, 3
  size: 103, 25
  orig: 103, 25
  offset: 0, 0
  index: -1
9
  rotate: true
  xy: 1134, 40
  size: 95, 45
  orig: 95, 45
  offset: 0, 0
  index: -1
toc
  rotate: true
  xy: 755, 97
  size: 113, 78
  orig: 113, 78
  offset: 0, 0
  index: -1
