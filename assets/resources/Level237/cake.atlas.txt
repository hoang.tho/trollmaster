
cake.png
size: 1176,2020
format: RGBA8888
filter: Linear,Linear
repeat: none
Cake cut
  rotate: false
  xy: 2, 1379
  size: 845, 636
  orig: 845, 636
  offset: 0, 0
  index: -1
arm
  rotate: false
  xy: 1137, 9
  size: 17, 255
  orig: 17, 255
  offset: 0, 0
  index: -1
bling3
  rotate: false
  xy: 968, 1805
  size: 29, 23
  orig: 29, 23
  offset: 0, 0
  index: -1
bling4
  rotate: true
  xy: 1127, 1960
  size: 55, 45
  orig: 55, 45
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 847, 1116
  size: 311, 347
  orig: 311, 347
  offset: 0, 0
  index: -1
cake full
  rotate: false
  xy: 2, 743
  size: 843, 634
  orig: 843, 634
  offset: 0, 0
  index: -1
eye3
  rotate: false
  xy: 968, 1643
  size: 105, 159
  orig: 105, 159
  offset: 0, 0
  index: -1
eye4
  rotate: false
  xy: 1056, 139
  size: 79, 120
  orig: 79, 120
  offset: 0, 0
  index: -1
eye_brown1
  rotate: false
  xy: 1015, 656
  size: 146, 107
  orig: 146, 107
  offset: 0, 0
  index: -1
eye_brown2
  rotate: false
  xy: 1051, 1530
  size: 91, 111
  orig: 91, 111
  offset: 0, 0
  index: -1
eye_brown3
  rotate: true
  xy: 1109, 1353
  size: 82, 51
  orig: 82, 51
  offset: 0, 0
  index: -1
eye_brown4
  rotate: true
  xy: 1122, 1064
  size: 79, 49
  orig: 79, 49
  offset: 0, 0
  index: -1
eye_pupil1
  rotate: false
  xy: 968, 1830
  size: 85, 185
  orig: 85, 185
  offset: 0, 0
  index: -1
eye_pupil2
  rotate: false
  xy: 1078, 8
  size: 56, 129
  orig: 56, 129
  offset: 0, 0
  index: -1
eye_sad1
  rotate: false
  xy: 847, 765
  size: 314, 357
  orig: 314, 357
  offset: 0, 0
  index: -1
eye_sad2
  rotate: false
  xy: 866, 517
  size: 147, 272
  orig: 147, 272
  offset: 0, 0
  index: -1
eye_suprise1
  rotate: true
  xy: 618, 2
  size: 295, 335
  orig: 295, 335
  offset: 0, 0
  index: -1
eye_suprise2
  rotate: false
  xy: 955, 261
  size: 156, 254
  orig: 156, 254
  offset: 0, 0
  index: -1
hair
  rotate: false
  xy: 849, 1486
  size: 200, 155
  orig: 200, 155
  offset: 0, 0
  index: -1
hand
  rotate: false
  xy: 1051, 1437
  size: 95, 91
  orig: 95, 91
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 2, 151
  size: 614, 590
  orig: 614, 590
  offset: 0, 0
  index: -1
leg
  rotate: false
  xy: 1093, 364
  size: 73, 205
  orig: 73, 205
  offset: 0, 0
  index: -1
mouth1
  rotate: true
  xy: 849, 1643
  size: 372, 117
  orig: 372, 117
  offset: 0, 0
  index: -1
mouth2
  rotate: false
  xy: 1015, 571
  size: 144, 83
  orig: 144, 83
  offset: 0, 0
  index: -1
mouth_laugh
  rotate: true
  xy: 618, 299
  size: 442, 246
  orig: 442, 246
  offset: 0, 0
  index: -1
nose
  rotate: true
  xy: 1055, 1873
  size: 142, 70
  orig: 142, 70
  offset: 0, 0
  index: -1
tear1
  rotate: true
  xy: 955, 26
  size: 233, 121
  orig: 233, 121
  offset: 0, 0
  index: -1
tear2
  rotate: true
  xy: 1113, 266
  size: 96, 52
  orig: 96, 52
  offset: 0, 0
  index: -1
thumb_up
  rotate: true
  xy: 1075, 1663
  size: 93, 88
  orig: 93, 88
  offset: 0, 0
  index: -1
toc
  rotate: true
  xy: 1075, 1758
  size: 113, 78
  orig: 113, 78
  offset: 0, 0
  index: -1
