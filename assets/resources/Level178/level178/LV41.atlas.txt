
LV41.png
size: 2044,736
format: RGBA8888
filter: Linear,Linear
repeat: none
Layer1
  rotate: false
  xy: 239, 69
  size: 217, 197
  orig: 217, 197
  offset: 0, 0
  index: -1
Layer10
  rotate: false
  xy: 1595, 50
  size: 163, 55
  orig: 163, 55
  offset: 0, 0
  index: -1
Layer11
  rotate: false
  xy: 707, 55
  size: 87, 51
  orig: 87, 51
  offset: 0, 0
  index: -1
Layer12
  rotate: true
  xy: 1153, 419
  size: 313, 258
  orig: 313, 258
  offset: 0, 0
  index: -1
Layer13
  rotate: false
  xy: 884, 125
  size: 224, 112
  orig: 224, 112
  offset: 0, 0
  index: -1
Layer14
  rotate: true
  xy: 458, 27
  size: 88, 127
  orig: 88, 127
  offset: 0, 0
  index: -1
Layer15
  rotate: true
  xy: 885, 31
  size: 92, 126
  orig: 92, 126
  offset: 0, 0
  index: -1
Layer16
  rotate: true
  xy: 1925, 77
  size: 239, 61
  orig: 239, 61
  offset: 0, 0
  index: -1
Layer17
  rotate: true
  xy: 138, 72
  size: 153, 94
  orig: 153, 94
  offset: 0, 0
  index: -1
Layer18
  rotate: true
  xy: 1265, 232
  size: 177, 245
  orig: 177, 245
  offset: 0, 0
  index: -1
Layer19
  rotate: true
  xy: 1512, 107
  size: 133, 232
  orig: 133, 232
  offset: 0, 0
  index: -1
Layer2
  rotate: true
  xy: 1591, 14
  size: 68, 143
  orig: 68, 143
  offset: 0, 0
  index: -1
Layer20
  rotate: false
  xy: 367, 268
  size: 285, 162
  orig: 285, 162
  offset: 0, 0
  index: -1
Layer21
  rotate: false
  xy: 1512, 256
  size: 112, 153
  orig: 112, 153
  offset: 0, 0
  index: -1
Layer22
  rotate: true
  xy: 458, 117
  size: 149, 183
  orig: 149, 183
  offset: 0, 0
  index: -1
Layer23
  rotate: true
  xy: 1449, 30
  size: 93, 161
  orig: 93, 161
  offset: 0, 0
  index: -1
Layer24
  rotate: false
  xy: 881, 240
  size: 108, 59
  orig: 108, 59
  offset: 0, 0
  index: -1
Layer25
  rotate: 180
  xy: 1423, 3
  size: 169, 106
  orig: 169, 106
  offset: 0, 0
  index: -1
Layer26
  rotate: false
  xy: 654, 346
  size: 68, 84
  orig: 68, 84
  offset: 0, 0
  index: -1
Layer27
  rotate: false
  xy: 1013, 35
  size: 94, 88
  orig: 94, 88
  offset: 0, 0
  index: -1
Layer28
  rotate: false
  xy: 1909, 47
  size: 112, 28
  orig: 112, 28
  offset: 0, 0
  index: -1
Layer29
  rotate: false
  xy: 796, 61
  size: 84, 45
  orig: 84, 45
  offset: 0, 0
  index: -1
Layer3
  rotate: false
  xy: 654, 308
  size: 81, 36
  orig: 81, 36
  offset: 0, 0
  index: -1
Layer30
  rotate: false
  xy: 1494, 72
  size: 50, 33
  orig: 50, 33
  offset: 0, 0
  index: -1
Layer31
  rotate: false
  xy: 748, 491
  size: 403, 241
  orig: 403, 241
  offset: 0, 0
  index: -1
Layer32
  rotate: false
  xy: 71, 35
  size: 151, 35
  orig: 151, 35
  offset: 0, 0
  index: -1
Layer33
  rotate: false
  xy: 2, 61
  size: 89, 44
  orig: 89, 44
  offset: 0, 0
  index: -1
Layer34
  rotate: true
  xy: 1413, 411
  size: 321, 249
  orig: 321, 249
  offset: 0, 0
  index: -1
Layer35
  rotate: false
  xy: 2, 418
  size: 363, 314
  orig: 363, 314
  offset: 0, 0
  index: -1
Layer36
  rotate: false
  xy: 1013, 239
  size: 250, 178
  orig: 250, 178
  offset: 0, 0
  index: -1
Layer37
  rotate: true
  xy: 1313, 112
  size: 118, 134
  orig: 118, 134
  offset: 0, 0
  index: -1
Layer38
  rotate: false
  xy: 1880, 457
  size: 137, 129
  orig: 137, 129
  offset: 0, 0
  index: -1
Layer39
  rotate: false
  xy: 587, 42
  size: 118, 64
  orig: 118, 64
  offset: 0, 0
  index: -1
Layer48
  rotate: false
  xy: 587, 42
  size: 118, 64
  orig: 118, 64
  offset: 0, 0
  index: -1
Layer4
  rotate: false
  xy: 1241, 48
  size: 50, 32
  orig: 50, 32
  offset: 0, 0
  index: -1
Layer40
  rotate: true
  xy: 1988, 68
  size: 248, 51
  orig: 248, 51
  offset: 0, 0
  index: -1
Layer41
  rotate: false
  xy: 654, 109
  size: 229, 196
  orig: 345, 334
  offset: 0, 138
  index: -1
Layer42
  rotate: false
  xy: 748, 301
  size: 263, 188
  orig: 263, 188
  offset: 0, 0
  index: -1
Layer43
  rotate: false
  xy: 239, 275
  size: 124, 141
  orig: 124, 141
  offset: 0, 0
  index: -1
Layer44
  rotate: true
  xy: 1880, 588
  size: 144, 136
  orig: 144, 136
  offset: 0, 0
  index: -1
Layer45
  rotate: false
  xy: 1013, 422
  size: 124, 67
  orig: 124, 67
  offset: 0, 0
  index: -1
Layer46
  rotate: true
  xy: 2, 107
  size: 118, 134
  orig: 118, 134
  offset: 0, 0
  index: -1
Layer47
  rotate: true
  xy: 1897, 318
  size: 137, 129
  orig: 137, 129
  offset: 0, 0
  index: -1
Layer49
  rotate: false
  xy: 2, 227
  size: 235, 189
  orig: 235, 189
  offset: 0, 0
  index: -1
Layer5
  rotate: true
  xy: 1110, 81
  size: 150, 201
  orig: 150, 201
  offset: 0, 0
  index: -1
Layer50
  rotate: false
  xy: 1746, 91
  size: 177, 149
  orig: 177, 149
  offset: 0, 0
  index: -1
Layer51
  rotate: false
  xy: 1449, 147
  size: 59, 83
  orig: 59, 83
  offset: 0, 0
  index: -1
Layer6
  rotate: true
  xy: 1664, 449
  size: 283, 214
  orig: 283, 214
  offset: 0, 0
  index: -1
Layer7
  rotate: false
  xy: 1664, 242
  size: 231, 205
  orig: 231, 205
  offset: 0, 0
  index: -1
Layer8
  rotate: true
  xy: 1752, 29
  size: 60, 163
  orig: 60, 163
  offset: 0, 0
  index: -1
Layer9
  rotate: false
  xy: 1109, 45
  size: 80, 36
  orig: 80, 36
  offset: 0, 0
  index: -1
toc
  rotate: false
  xy: 1293, 32
  size: 113, 78
  orig: 113, 78
  offset: 0, 0
  index: -1
tocccc
  rotate: false
  xy: 367, 432
  size: 379, 300
  orig: 379, 300
  offset: 0, 0
  index: -1
