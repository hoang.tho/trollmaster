
pencil_money.png
size: 1360,288
format: RGBA8888
filter: Linear,Linear
repeat: none
$
  rotate: false
  xy: 2, 19
  size: 349, 266
  orig: 349, 266
  offset: 0, 0
  index: -1
L_eye
  rotate: false
  xy: 1161, 12
  size: 83, 106
  orig: 83, 106
  offset: 0, 0
  index: -1
L_eye2
  rotate: false
  xy: 1074, 3
  size: 85, 115
  orig: 85, 115
  offset: 0, 0
  index: -1
R-eye2
  rotate: false
  xy: 1013, 120
  size: 108, 116
  orig: 108, 116
  offset: 0, 0
  index: -1
R_eye
  rotate: false
  xy: 896, 160
  size: 115, 125
  orig: 115, 125
  offset: 0, 0
  index: -1
R_leg
  rotate: 180
  xy: 1208, 126
  size: 65, 161
  orig: 65, 161
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 729, 93
  size: 165, 192
  orig: 165, 192
  offset: 0, 0
  index: -1
bubble_talk
  rotate: true
  xy: 729, 3
  size: 88, 159
  orig: 88, 159
  offset: 0, 0
  index: -1
canh tay Copy
  rotate: true
  xy: 1271, 66
  size: 196, 59
  orig: 196, 59
  offset: 0, 0
  index: -1
con nguoi
  rotate: false
  xy: 1246, 16
  size: 18, 18
  orig: 18, 18
  offset: 0, 0
  index: -1
eye_brown
  rotate: false
  xy: 1025, 17
  size: 40, 28
  orig: 40, 28
  offset: 0, 0
  index: -1
eye_brown1
  rotate: false
  xy: 890, 5
  size: 133, 40
  orig: 133, 40
  offset: 0, 0
  index: -1
face
  rotate: true
  xy: 353, 13
  size: 272, 240
  orig: 272, 240
  offset: 0, 0
  index: -1
hand
  rotate: 270
  xy: 1298, 219
  size: 66, 49
  orig: 66, 49
  offset: 0, 0
  index: -1
hand2
  rotate: false
  xy: 1278, 8
  size: 51, 68
  orig: 78, 110
  offset: 0, 42
  index: -1
mieng
  rotate: false
  xy: 890, 47
  size: 182, 71
  orig: 182, 71
  offset: 0, 0
  index: -1
money
  rotate: 180
  xy: 1154, 210
  size: 104, 75
  orig: 104, 75
  offset: 0, 0
  index: -1
mouth_smile
  rotate: true
  xy: 595, 13
  size: 227, 132
  orig: 227, 132
  offset: 0, 0
  index: -1
tear
  rotate: true
  xy: 1246, 36
  size: 88, 30
  orig: 88, 30
  offset: 0, 0
  index: -1
tear2
  rotate: true
  xy: 1330, 5
  size: 58, 25
  orig: 58, 25
  offset: 0, 0
  index: -1
toc
  rotate: true
  xy: 1123, 120
  size: 113, 78
  orig: 113, 78
  offset: 0, 0
  index: -1
