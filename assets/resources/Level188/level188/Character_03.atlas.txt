
Character_03.png
size: 2024,624
format: RGBA8888
filter: Linear,Linear
repeat: none
Layer100
  rotate: false
  xy: 491, 141
  size: 101, 60
  orig: 101, 60
  offset: 0, 0
  index: -1
layer10
  rotate: false
  xy: 1744, 124
  size: 69, 58
  orig: 69, 58
  offset: 0, 0
  index: -1
layer11
  rotate: true
  xy: 1546, 392
  size: 229, 202
  orig: 229, 202
  offset: 0, 0
  index: -1
layer12
  rotate: false
  xy: 1750, 184
  size: 108, 237
  orig: 108, 237
  offset: 0, 0
  index: -1
layer14
  rotate: false
  xy: 1383, 302
  size: 161, 319
  orig: 161, 319
  offset: 0, 0
  index: -1
layer15
  rotate: false
  xy: 275, 97
  size: 472, 105
  orig: 473, 105
  offset: 0, 0
  index: -1
layer17
  rotate: true
  xy: 1230, 3
  size: 190, 258
  orig: 190, 258
  offset: 0, 0
  index: -1
layer18
  rotate: true
  xy: 1945, 522
  size: 99, 76
  orig: 99, 76
  offset: 0, 0
  index: -1
layer21
  rotate: false
  xy: 1125, 160
  size: 121, 83
  orig: 121, 83
  offset: 0, 0
  index: -1
layer22
  rotate: true
  xy: 1367, 193
  size: 107, 235
  orig: 107, 235
  offset: 0, 0
  index: -1
layer25
  rotate: false
  xy: 1604, 267
  size: 143, 123
  orig: 143, 123
  offset: 0, 0
  index: -1
layer26
  rotate: false
  xy: 1860, 133
  size: 146, 115
  orig: 146, 115
  offset: 0, 0
  index: -1
layer27
  rotate: true
  xy: 2, 127
  size: 74, 271
  orig: 74, 271
  offset: 0, 0
  index: -1
layer3
  rotate: true
  xy: 1125, 245
  size: 376, 256
  orig: 376, 256
  offset: 0, 0
  index: -1
layer31
  rotate: false
  xy: 2, 203
  size: 619, 418
  orig: 619, 418
  offset: 0, 0
  index: -1
layer4
  rotate: true
  xy: 623, 175
  size: 446, 500
  orig: 446, 500
  offset: 0, 0
  index: -1
layer5
  rotate: false
  xy: 1750, 423
  size: 193, 198
  orig: 193, 198
  offset: 0, 0
  index: -1
layer6
  rotate: false
  xy: 1945, 428
  size: 73, 92
  orig: 73, 92
  offset: 0, 0
  index: -1
layer7
  rotate: false
  xy: 1125, 581
  size: 45, 40
  orig: 45, 40
  offset: 0, 0
  index: -1
layer8
  rotate: true
  xy: 1860, 250
  size: 171, 123
  orig: 171, 123
  offset: 0, 0
  index: -1
layer9
  rotate: false
  xy: 1604, 152
  size: 138, 113
  orig: 138, 113
  offset: 0, 0
  index: -1
snow
  rotate: false
  xy: 1488, 126
  size: 65, 65
  orig: 65, 65
  offset: 0, 0
  index: -1
toc
  rotate: false
  xy: 1248, 165
  size: 113, 78
  orig: 113, 78
  offset: 0, 0
  index: -1
