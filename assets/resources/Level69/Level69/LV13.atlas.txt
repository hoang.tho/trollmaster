
LV13.png
size: 1836,716
format: RGBA8888
filter: Linear,Linear
repeat: none
Layer1
  rotate: false
  xy: 2, 431
  size: 760, 282
  orig: 760, 282
  offset: 0, 0
  index: -1
Layer10
  rotate: false
  xy: 1267, 24
  size: 158, 73
  orig: 158, 73
  offset: 0, 0
  index: -1
Layer11
  rotate: false
  xy: 764, 273
  size: 448, 440
  orig: 448, 440
  offset: 0, 0
  index: -1
Layer12
  rotate: false
  xy: 380, 355
  size: 272, 74
  orig: 272, 74
  offset: 0, 0
  index: -1
Layer13
  rotate: false
  xy: 889, 58
  size: 205, 213
  orig: 205, 213
  offset: 0, 0
  index: -1
Layer14
  rotate: false
  xy: 1767, 110
  size: 64, 95
  orig: 64, 95
  offset: 0, 0
  index: -1
Layer15
  rotate: false
  xy: 399, 5
  size: 80, 80
  orig: 80, 80
  offset: 0, 0
  index: -1
Layer16
  rotate: true
  xy: 1649, 155
  size: 558, 129
  orig: 558, 129
  offset: 0, 0
  index: -1
Layer17
  rotate: false
  xy: 1517, 95
  size: 99, 113
  orig: 99, 113
  offset: 0, 0
  index: -1
Layer18
  rotate: false
  xy: 1267, 99
  size: 248, 127
  orig: 248, 127
  offset: 0, 0
  index: -1
Layer19
  rotate: false
  xy: 1096, 35
  size: 169, 230
  orig: 169, 230
  offset: 0, 0
  index: -1
Layer2
  rotate: false
  xy: 1214, 210
  size: 524, 503
  orig: 524, 503
  offset: 0, 0
  index: -1
Layer20
  rotate: false
  xy: 1760, 207
  size: 56, 98
  orig: 56, 98
  offset: 0, 0
  index: -1
Layer21
  rotate: false
  xy: 2, 8
  size: 92, 64
  orig: 92, 64
  offset: 0, 0
  index: -1
Layer3
  rotate: false
  xy: 2, 6
  size: 467, 423
  orig: 467, 423
  offset: 0, 0
  index: -1
Layer4
  rotate: 180
  xy: 1749, 343
  size: 83, 318
  orig: 83, 318
  offset: 0, 0
  index: -1
Layer5
  rotate: false
  xy: 1701, 49
  size: 129, 59
  orig: 129, 59
  offset: 0, 0
  index: -1
Layer6
  rotate: 270
  xy: 1621, 4
  size: 200, 189
  orig: 200, 189
  offset: 0, 0
  index: -1
Layer7
  rotate: false
  xy: 1542, 21
  size: 52, 72
  orig: 52, 72
  offset: 0, 0
  index: -1
Layer8
  rotate: 270
  xy: 454, 42
  size: 278, 434
  orig: 278, 434
  offset: 0, 0
  index: -1
Layer9
  rotate: 270
  xy: 518, 3
  size: 102, 368
  orig: 102, 368
  offset: 0, 0
  index: -1
toc
  rotate: false
  xy: 1427, 15
  size: 113, 78
  orig: 113, 78
  offset: 0, 0
  index: -1
