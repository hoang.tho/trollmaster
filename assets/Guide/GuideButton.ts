import Guide from "./Guide";

const {ccclass, property} = cc._decorator;

@ccclass
export default class GuideButton extends cc.Component {

    @property(cc.Node)
    next: cc.Node = null;

    onEnable(): void {
        this.node.on(cc.Node.EventType.TOUCH_END, () => {
            if (this.next) {
                this.next.active = true;
            }
            else
            {
                Guide.getInstance().showWelcome();
            }
            this.node.parent.active = false;
        }, this);
    }
}
