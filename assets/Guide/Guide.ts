import GameManager from "../Script/GameManager";
import UIController from "../Script/UIController";
import { GroupUI } from "../Script/UIController";


const {ccclass, property} = cc._decorator;

const anim = {
    guide1: "Guide",
    guide2: "Guide2"
}
@ccclass
export default class Guide extends cc.Component {

    @property(cc.Node)
    pencil: cc.Node = null;

    @property(cc.Node)
    lv1: cc.Node = null;

    @property(cc.Node)
    lv2: cc.Node = null;

    @property(cc.Node)
    imageLv2: cc.Node = null;

    @property(cc.Node)
    shadow: cc.Node = null;

    @property(cc.Node)
    textGuide2: cc.Node = null;

    @property([cc.Animation])
    topButton: cc.Animation[] = [];

    @property([cc.Animation])
    botButton: cc.Animation[] = [];

    @property(cc.Node)
    hint: cc.Node = null;

    @property(cc.Node)
    startBtn: cc.Node = null;

    @property(cc.Node)
    txt: cc.Node = null;

    @property([cc.Node])
    guide: cc.Node[] = [];

    private static _instance;

    public static getInstance(): Guide {
        if (!Guide._instance) {
            Guide._instance = this;
        }
        return Guide._instance;
    }

    onLoad(): void {
        Guide._instance = this;
    }

    onEnable(): void {
        this.imageLv2.active = false;
        this.node.on("onfinish", this.onFinish, this);
        this.shadow.active = false;
        this.pencil.getComponent(cc.Animation).play(anim.guide1);
        this.textGuide2.active = false;
        this.pencil.active = true;
        this.topButton.forEach(element => {
            element.play("show_top");
        });
        this.botButton.forEach(element => {
            element.play("show_bot");
        });
        this.hint.active = false;
        this.startBtn.active = false;
        this.txt.active = false;
        this.startBtn.active = false;
        this.guide.forEach(element => {
            element.active = false;
        });
        this.node.on(cc.Node.EventType.TOUCH_START, this.hidePencil, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.showPencil, this);
    }

    hidePencil(): void {
        this.pencil.active = false;
    }

    showPencil(): void {
        this.pencil.active = true;
    }

    start(): void {
        this.lv2.active = false;
        this.lv1.active = true;
    }

    onDisable(): void {
        UIController.getInstance().showUI(GroupUI.InGame, true);
        cc.find("Canvas/GameContainer").children[0].active = true;
    }

    showHintGuide(event): void {
        this.lv2.getComponent("LevelController").onHint();
        this.lv2.getComponent("LevelController")._hint = true;
        this.imageLv2.active = true;
        this.shadow.active = false;
        this.textGuide2.active = false;
        this.pencil.getComponent(cc.Animation).stop();
        this.pencil.getComponent(cc.Animation).setCurrentTime(0, anim.guide2);
    }

    onFinish(event): void {
        if (this.lv1.active) {
            this.node.off(cc.Node.EventType.TOUCH_START, this.hidePencil, this);
            this.node.off(cc.Node.EventType.TOUCH_START, this.showPencil, this);
            this.pencil.active = false;
            this.topButton.forEach(element => {
                element.play("hide_top");
            });
            this.botButton.forEach(element => {
                element.play("hide_bot");
            });
            this.scheduleOnce(() => {

                this.topButton.forEach(element => {
                    element.play("show_top");
                });
                this.botButton.forEach(element => {
                    element.play("show_bot");
                });
                this.hint.active = true;

                let animState = this.hint.getComponent(cc.Animation).play("show_hint");

                this.lv1.getChildByName("Block").active = true;
                this.lv1.active = false;
                this.lv2.active = true;

                this.scheduleOnce(() => {
                    this.shadow.active = true;
                }, 0);
                this.scheduleOnce(() => {
                    this.pencil.active = true;
                    this.textGuide2.active = true;
                }, 1);
            }, 2);
        }
        else {
            this.topButton.forEach(element => {
                element.play("hide_top");
            });
            this.botButton.forEach(element => {
                element.play("hide_bot");
            });
            this.scheduleOnce(() => {
                this.topButton.forEach(element => {
                    element.play("show_top");
                });
                this.botButton.forEach(element => {
                    element.play("show_bot");
                });
                
                this.scheduleOnce(() => {
                    this.showGuideButton();
                }, 1);
                
            }, 2);
        }
    }

    startGame(): void {
        GameManager.getInstance()._block = false;
        this.node.active = false;
    }

    showGuideButton(): void {
        this.guide[0].active = true;
    }

    showWelcome(): void {
        this.lv2.active = false;
        this.scheduleOnce(() => {
            this.txt.active = true;
            this.scheduleOnce(() => {
                this.startBtn.active = true;
            }, 1);
        }, 1);
    }

    update(): void {
        this.hint.x = cc.view.getCanvasSize().width / 2 - 50;
    }
}
