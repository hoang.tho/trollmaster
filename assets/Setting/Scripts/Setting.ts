import Panel from "../../Script/Panel";
import Button from "./Button";
import { KEY } from "../../Script/Data";
import GameManager from "../../Script/GameManager";


const {ccclass, property} = cc._decorator;

@ccclass
export default class Setting extends Panel {

    @property(Button)
    music: Button = null;

    @property(Button)
    sound: Button = null;

    @property(Button)
    vibrate: Button = null;

    onEnable(): void {
        super.onEnable();
        this.node.on("change_setting", this.onChange, this);
        this.music.setState(this.userData.isMusic);
        this.sound.setState(this.userData.isSound);
        this.vibrate.setState(this.userData.isVibrate);
    }

    onChange(event): void {
        switch (event.nameTarget)
        {
            case "music":
                this.userData.isMusic = event.state;
                event.state ? GameManager.getInstance().playBG() : cc.audioEngine.pauseMusic();
                break;
            case "sound":
                this.userData.isSound = event.state;
                break;
            case "vibrate":
                this.userData.isVibrate = event.state;
                break;
        }
        GameManager.userData = this.userData;
        this.saveData(KEY.LOCAL, this.userData);
    }
}
