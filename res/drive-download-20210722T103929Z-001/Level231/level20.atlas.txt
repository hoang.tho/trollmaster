
level20.png
size: 1008,908
format: RGBA8888
filter: Linear,Linear
repeat: none
banchan1
  rotate: false
  xy: 163, 881
  size: 53, 24
  orig: 53, 24
  offset: 0, 0
  index: -1
banchan2
  rotate: false
  xy: 218, 881
  size: 50, 24
  orig: 50, 24
  offset: 0, 0
  index: -1
bantay1
  rotate: true
  xy: 955, 855
  size: 52, 49
  orig: 54, 51
  offset: 1, 1
  index: -1
body
  rotate: false
  xy: 2, 780
  size: 66, 126
  orig: 66, 126
  offset: 0, 0
  index: -1
canhtay1
  rotate: false
  xy: 2, 738
  size: 68, 62
  orig: 68, 62
  offset: 0, 0
  index: -1
canhtay2
  rotate: false
  xy: 17, 691
  size: 68, 62
  orig: 69, 62
  offset: 0, 0
  index: -1
chan1
  rotate: false
  xy: 955, 785
  size: 27, 81
  orig: 29, 82
  offset: 1, 1
  index: -1
chan2
  rotate: true
  xy: 290, 828
  size: 27, 82
  orig: 28, 82
  offset: 0, 0
  index: -1
dau
  rotate: false
  xy: 2, 112
  size: 173, 172
  orig: 173, 172
  offset: 0, 0
  index: -1
effect/dead01
  rotate: true
  xy: 535, 587
  size: 318, 276
  orig: 318, 276
  offset: 0, 0
  index: -1
effect/dead02
  rotate: false
  xy: 397, 309
  size: 318, 276
  orig: 318, 276
  offset: 0, 0
  index: -1
effect/dead03
  rotate: false
  xy: 2, 285
  size: 318, 276
  orig: 318, 276
  offset: 0, 0
  index: -1
effect/dead04
  rotate: true
  xy: 717, 267
  size: 318, 276
  orig: 318, 276
  offset: 0, 0
  index: -1
effect/dead05
  rotate: false
  xy: 322, 31
  size: 318, 276
  orig: 318, 276
  offset: 0, 0
  index: -1
img2/dauthan
  rotate: true
  xy: 813, 707
  size: 198, 140
  orig: 198, 140
  offset: 0, 0
  index: -1
img2/den
  rotate: true
  xy: 177, 83
  size: 200, 110
  orig: 200, 110
  offset: 0, 0
  index: -1
img2/duoithan
  rotate: false
  xy: 2, 4
  size: 202, 77
  orig: 202, 77
  offset: 0, 0
  index: -1
img2/mat1
  rotate: false
  xy: 844, 152
  size: 139, 113
  orig: 139, 113
  offset: 0, 0
  index: -1
img2/mat5
  rotate: false
  xy: 844, 70
  size: 157, 80
  orig: 157, 80
  offset: 0, 0
  index: -1
img2/matthan
  rotate: false
  xy: 278, 856
  size: 110, 49
  orig: 110, 49
  offset: 0, 0
  index: -1
img2/mieng5
  rotate: true
  xy: 642, 67
  size: 198, 99
  orig: 198, 99
  offset: 0, 0
  index: -1
img2/momthan
  rotate: false
  xy: 57, 754
  size: 134, 45
  orig: 134, 45
  offset: 0, 0
  index: -1
img2/nguoithan
  rotate: false
  xy: 2, 563
  size: 532, 342
  orig: 532, 342
  offset: 0, 0
  index: -1
img2/taythan1
  rotate: true
  xy: 743, 77
  size: 188, 99
  orig: 188, 99
  offset: 0, 0
  index: -1
img2/taythan2
  rotate: false
  xy: 813, 590
  size: 156, 115
  orig: 156, 115
  offset: 0, 0
  index: -1
img2/tocthan
  rotate: false
  xy: 67, 851
  size: 96, 55
  orig: 96, 55
  offset: 0, 0
  index: -1
shadow2
  rotate: false
  xy: 743, 31
  size: 221, 37
  orig: 221, 37
  offset: 0, 0
  index: -1
toc
  rotate: false
  xy: 206, 3
  size: 113, 78
  orig: 113, 78
  offset: 0, 0
  index: -1
