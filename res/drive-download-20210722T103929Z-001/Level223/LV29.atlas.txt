
LV29.png
size: 926,478
format: RGBA8888
filter: Linear,Linear
repeat: none
body_bi
  rotate: true
  xy: 2, 11
  size: 465, 305
  orig: 467, 307
  offset: 1, 1
  index: -1
body_bi_back
  rotate: true
  xy: 309, 39
  size: 437, 307
  orig: 439, 309
  offset: 1, 1
  index: -1
body_cat
  rotate: true
  xy: 618, 175
  size: 301, 306
  orig: 303, 308
  offset: 1, 1
  index: -1
eye
  rotate: false
  xy: 309, 2
  size: 124, 35
  orig: 126, 37
  offset: 1, 1
  index: -1
hand_l
  rotate: false
  xy: 845, 107
  size: 67, 66
  orig: 69, 68
  offset: 1, 1
  index: -1
hand_r
  rotate: false
  xy: 845, 39
  size: 67, 66
  orig: 69, 68
  offset: 1, 1
  index: -1
head_bi
  rotate: false
  xy: 618, 42
  size: 225, 131
  orig: 227, 133
  offset: 1, 1
  index: -1
