
mode draw4.png
size: 620,520
format: RGBA8888
filter: Linear,Linear
repeat: none
body
  rotate: false
  xy: 2, 100
  size: 211, 416
  orig: 211, 416
  offset: 0, 0
  index: -1
canh1
  rotate: 270
  xy: 202, 77
  size: 210, 309
  orig: 210, 309
  offset: 0, 0
  index: -1
canh2
  rotate: false
  xy: 173, 167
  size: 270, 344
  orig: 271, 344
  offset: 0, 0
  index: -1
chan1_1
  rotate: false
  xy: 419, 3
  size: 114, 84
  orig: 114, 84
  offset: 0, 0
  index: -1
chan1_2
  rotate: false
  xy: 2, 142
  size: 65, 104
  orig: 66, 106
  offset: 0, 1
  index: -1
chan2_1
  rotate: false
  xy: 512, 84
  size: 103, 121
  orig: 105, 122
  offset: 1, 1
  index: -1
chan2_2
  rotate: false
  xy: 445, 271
  size: 124, 76
  orig: 124, 76
  offset: 0, 0
  index: -1
duoi1
  rotate: true
  xy: 528, 349
  size: 100, 70
  orig: 100, 70
  offset: 0, 0
  index: -1
duoi2
  rotate: true
  xy: 395, 360
  size: 157, 138
  orig: 157, 138
  offset: 0, 0
  index: -1
mao
  rotate: false
  xy: 34, 36
  size: 186, 126
  orig: 186, 127
  offset: 0, 0
  index: -1
mat1
  rotate: false
  xy: 319, 17
  size: 46, 66
  orig: 46, 66
  offset: 0, 0
  index: -1
mat2
  rotate: false
  xy: 174, 284
  size: 57, 61
  orig: 57, 61
  offset: 0, 0
  index: -1
mo_tren
  rotate: true
  xy: 429, 153
  size: 116, 120
  orig: 116, 120
  offset: 0, 0
  index: -1
moduoi
  rotate: true
  xy: 208, 4
  size: 89, 109
  orig: 89, 109
  offset: 0, 0
  index: -1
