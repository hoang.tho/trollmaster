
LV24.png
size: 751,377
format: RGBA8888
filter: Linear,Linear
repeat: none
body
  rotate: false
  xy: 2, 2
  size: 454, 373
  orig: 454, 373
  offset: 0, 0
  index: -1
eye_1
  rotate: false
  xy: 522, 18
  size: 68, 34
  orig: 70, 36
  offset: 1, 1
  index: -1
eye_2
  rotate: false
  xy: 592, 27
  size: 62, 29
  orig: 64, 31
  offset: 1, 1
  index: -1
light
  rotate: true
  xy: 458, 179
  size: 196, 201
  orig: 198, 203
  offset: 1, 1
  index: -1
mo_hoi
  rotate: false
  xy: 739, 238
  size: 10, 24
  orig: 12, 26
  offset: 1, 1
  index: -1
mouth_1
  rotate: false
  xy: 522, 58
  size: 137, 119
  orig: 139, 121
  offset: 1, 1
  index: -1
mouth_2
  rotate: true
  xy: 458, 23
  size: 139, 62
  orig: 141, 64
  offset: 1, 1
  index: -1
mui
  rotate: false
  xy: 458, 3
  size: 32, 18
  orig: 34, 20
  offset: 1, 1
  index: -1
rau
  rotate: true
  xy: 661, 264
  size: 111, 85
  orig: 111, 85
  offset: 0, 0
  index: -1
vay_1
  rotate: false
  xy: 661, 54
  size: 72, 108
  orig: 74, 110
  offset: 1, 1
  index: -1
vay_2
  rotate: true
  xy: 661, 164
  size: 98, 76
  orig: 100, 78
  offset: 1, 1
  index: -1
